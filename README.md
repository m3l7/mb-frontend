### MB frontend

Frontend of MB (mb-fix.it) project. Relevant libraries used are:

* angularjs
* scss
* js-data as model layer
* $httpBackend
* bootstrap
* angular translate

## Install

clone the repo and install the dependencies:

      npm install
      bower install

## Configure

Edit app/app.config.js:

      API.baseUrl: url of the main API endpoint

## Dev mode

Run grunt to start a development server with livereload and scss/jade/js live compiling:

      grunt serve

##### Backendless development

You can serve the project with a fake (using $httpBackend) server:

      MOCK=true grunt serve

The requests/responses configurations are stored in app/mock/mockbackend.js

## Deployment

### Build

Build the project with grunt:

      grunt build

The compiled version will be found in dist/ folder.

##### Disable uglify

You can disable uglification of js sources (for debugging purposes) with:

      UGLIFY=false grunt build

### Production mode

Use deyob-frontend.js to start a static server which serves prod/ (default) or dist/ folder:

      node deyob-frontend.js
      NODE_ENV=dist node mbfrontend.js

Prepend a PORT env variable to change default port (19017):

      PORT=14055 node mbfrontend.js


## Folders and files Structure

* `app.config.js`		main config file
* `app.route.js` 		routing
* `app.module` module and dependencies definition

#### admin/

login page

#### components/

directives and some 3rd party libraries (which are not bowerizable)

#### content/

scss styles and images

#### filters/

angularjs filters


#### languages/

Language definition for angular translate

* `italian.config.js` main angular translate config file with default italian json file
* `langdetect.run.js` detect the browser language and try to set a proper language if found
* `*.json` other language definitions

#### layout/

layout views/controllers

* `header` main top bar
* `nav` main side bar

#### log/

logging services

* `log.http-interceptor.js` intercept responses from server and write to console ir log.service if needed.
* `log.service.js` basic log store for errors responses from server

#### mock/

$httpBackend definitions for backendless development (or integration tests).

* `mockbackend.empty` dummy module for bypassing httpbackend when not needed
* testData.js` test datas used for faked responses

#### models/

js-data models definitions

#### pages/

main views/controllers

#### security/

security related services

* `auth.http-interceptor.js` intercept unathorized responses from server (i.e. 401) and redirect to login page
* `auth.routes.js` check when a view is loaded if the user is authorized for the view's role (if the route has a role set - see app.routes.js)
* `security.service.js` login/logout methods. Auth info are stored into localStorage

#### services/

Other services

* `commonData.service.js` super useful empty service (please delete me)
* `langUtils.service.js` store the current language and exports a changeLang method