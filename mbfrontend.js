var express = require('express'),
http = require('http'),
path = require('path'),
url = require("url"),
filesys = require("fs"),
cors = require('cors'),
os = require('os'),
app = express();

app.use(cors());

// SETUP THE HOST
var prod_port = 19017;

// MAILGUN IMPORT AND CONFIGURATION
var mailgunAPIkey = 'key-fee6b455d644bedebf9f5a53a08feaf7';
var mailgunUser = 'postmaster@mb-fix.com';
var mailgunDomain = 'mb-fix.com';
var mailgunPassword = '37c2f4b5887aa88f489b5a13fcbdeedf';
var mailgunHost = 'smtp.mailgun.org';


app.post('/sendmail' ,function(req, res) {
    var data = "";
    req.on('data', function (chunk) {
        data += chunk;
    })
    req.on('end', function () {
        data = JSON.parse(data);
        launchmail(data.mail, data.companyName, data.invoiceAddress, data.VATcode, data.phone, res);
    })
});

var mailgun = require('mailgun-js')({
    apiKey : mailgunAPIkey,
    domain : mailgunDomain
});

// all environments
app.set('port', process.env.PORT || prod_port);


app.use(function(req, res, next) {
    console.log("DEBUG\t --- Requested original url ", req.url);
    if (req.originalUrl.indexOf('sendmail') >= 0) {
        console.log(req.query);
        var data = "";
        req.on('data', function(chunk) {
            data += chunk;
            console.log('data chunk: ', data);
        });
        req.on('end', function() {
            console.log('mail data ', data);
            data = JSON.parse(data);
            var mail = data.mail;
            var companyName = data.companyName;
            var VATcode = data.VATcode;
            var phone = data.phone;
            var invoiceAddress = data.invoiceAddress;
            launchmail(mail, companyName, invoiceAddress, VATcode, phone, res);
        })
    } else if (req.originalUrl.indexOf('_escaped_fragment_') >= 0 && req.url.match(/(jpg|jpeg|png|css|js|woff|eot|ttf)/g) === null) {
        console.log('----> RENDERING FOR BOT');
        getPagesForBot(req, res, next);
    } else {
        console.log('----> RENDERING FOR HUMAN');
        req.url = req.url.replace('?_escaped_fragment_=/', '');
        if (app.get('env') == 'dist') {
            app.use(express.static(path.join(__dirname, './dist')));
            app.use('./dist/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        } else if (app.get('env') == 'prod') {
            app.use(express.static(path.join(__dirname, './prod')));
            app.use('./prod/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        } else {
            app.use(express.static(path.join(__dirname, './prod')));
            app.use('./prod/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        }
    }
});

app.post('/sendmail', function(req, res, next) {
    var data = "";
    console.log(req.query);
    req.on('data', function(chunk) {
        data += chunk;
        console.log('data chunk: ', data);
    });
    req.on('end', function() {
        console.log('mail data ', data);
        data = JSON.parse(data);
        var mail = data.mail;
        var companyName = data.companyName;
        var VATcode = data.VATcode;
        var phone = data.phone;
        var invoiceAddress = data.invoiceAddress;
        launchmail(mail, companyName, invoiceAddress, VATcode, phone, res);
    })
});

http.createServer(app).listen(app.get('port'), function() {
    console.log("Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});

function launchmail(email, azienda, indirizzoFatturazione, piva, phone, res) {
    // console.log('inside launchmail function');
    var mbFixEmail = 'noreply@mb-fix.com';
    var data = {};
    var sentFrom = "";

    var body = "<h2>Richiesta registrazione nuovo utente</h2>";
    body += "<p><strong>" + azienda + "</strong> ha richiesto di poter accedere all'<a href='http://mb-fix.it/#!/login'>area clienti</a></p>";
    body += "<h3>Dati del cliente:</h3>";
    body += "<p><strong>Azienda</strong> " + azienda + "</p>";
    body += "<p><strong>Email:</strong> " + email + "</p>";
    body += "<p><strong>Indirizzo di fatturazione:</strong> " + indirizzoFatturazione + "</p>";
    body += "<p><strong>Partita Iva:</strong> " + piva + "</p>";
    body += "<p><strong>Telefono:</strong> " + phone + "</p>";

    var text = azienda + " ha richiesto di poter accedere all'area clienti";

    data = {
        from : 'MB-FIX - WEB CONTACT <noreply@mb-fix.it>',
        to : 'marketing@mb-fix.it',
        subject : 'Richiesta Registrazione Nuovo Cliente',
        text : text,
        html : body
    };


    mailgun.messages().send(data, function(error, body) {
        // console.log(data);
        if (error) {
            console.log("MAILGUN ERROR: ", error);
            res.render('error', {
                error : error
            });
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.set({
                "Content-Type" : "text/plain; charset=UTF-8"
            });
            res.status(500).send('An error occourred... Please, try later.');
        } else {
            // console.log(body);
            // console.log('Message sent ', body.response);
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.set({
                "Content-Type" : "text/plain; charset=UTF-8"
            });
            res.status(200).send('Mail sent to MB-FIX!');
        }
    });
}


function getPagesForBot(request, response, next) {
    var full_path = thePath(request, response);

    // SANITIZE PATH
    console.log('DEBUG SEO\t --- requested url', full_path)
    full_path = full_path.replace('#!/', '');

    if (request.url.indexOf('/catalogue') >= 0) {
        full_path = full_path.replace('catalogue', 'snapshots/catalogue/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/category') >= 0) {
        full_path = full_path.replace('category', 'snapshots/category/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/contacts') >= 0) {
        full_path = full_path.replace('contacts', 'snapshots/contacts/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/categories') >= 0) {
        full_path = full_path.replace('categories', 'snapshots/categories/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/certificates') >= 0) {
        full_path = full_path.replace('certificates', 'snapshots/certificates/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/products') >= 0) {
        full_path = full_path.replace('products', 'snapshots/products/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/services') >= 0) {
        full_path = full_path.replace('services', 'snapshots/services/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vdi-matrix') >= 0) {
        full_path = full_path.replace('vdi-matrix', 'snapshots/vdi-matrix/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vdi') >= 0) {
        full_path = full_path.replace('vdi', 'snapshots/vdi/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/video') >= 0) {
        full_path = full_path.replace('video', 'snapshots/video/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vision') >= 0) {
        full_path = full_path.replace('vision', 'snapshots/vision/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url === '/') {
        full_path += 'snapshots/index.html';
    }
    console.log("DEBUG SEO\t --- Rendered url ", full_path)
    getFile(request, response, next, full_path)
}

function getFile(request, response, next, path) {
    var n = path.indexOf('?');
    var full_path = path.substring(0, n != -1 ? n : path.length);

    filesys.readFile(path, 'utf8', function(err, data) {
        if (err) {
            console.log('DEBUG ERROR\t --- ', err);
            response.sendStatus(500);
            response.end();
        } else {
            filesys.exists(full_path, function(exists) {
                if (!exists) {
                    console.log('DEBUG NOT FOUND\t --- file does not exist');
                    response.sendStatus(404);
                    response.end();
                } else {
                    var file = data;
                    response.status(200);
                    response.send(file);
                    response.end();
                }
            })
        }
    })
}

function thePath(request, response) {
    console.log("PATH BEFORE\t --- original full_path " + request.url + " with method " + request.method);
    request.url = request.url.replace('?_escaped_fragment_=/', '');
    var newpath = url.parse(request.url).pathname;
    var folder = app.get('env') === 'dist' ? '/dist' : '/prod';
    newpath = folder + newpath;
    var full_path = path.join(process.cwd(), newpath);
    console.log("PATH AFTER\t --- Created full_path " + full_path + " with method " + request.method);

    return full_path;
}