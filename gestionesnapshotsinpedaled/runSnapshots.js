// require('child_process').exec('grunt snapshots')
var spawn = require('child_process').spawn;

run();

function run(){

	var gruntInstance = spawn('grunt',['snapshots']);
	gruntInstance.stdout.on('data', function (data) {
	  gruntTerminated = false;
	  console.log(new Date()+ " GruntCron snapshots: "+data);
	});

	gruntInstance.stdout.on('close', function () {
	  gruntTerminated = true;
	  console.log("Snapshots completed ");
	  process.exit();
	});

}
