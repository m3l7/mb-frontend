var express = require('express'),
http = require('http'),
path = require('path'),
// bodyParser = require('body-parser'),
url = require("url"),
filesys = require("fs"),
request = require('request'),
jsdom = require('jsdom'),
os = require('os'),
renderer = require('angular-jsdom-renderer'),
app = express();


// MAILGUN IMPORT AND CONFIGURATION
var mailgunAPIkey = 'key-fee6b455d644bedebf9f5a53a08feaf7';
var mailgunUser = 'postmaster@sandbox470e6f41e2444c1daeeeffea13957418.mailgun.org';
var mailgunDomain = 'sandbox470e6f41e2444c1daeeeffea13957418.mailgun.org';
var mailgunPassword = '554abc87cce69b762cb0e81d0c59255f';
var mailgunHost = 'smtp.mailgun.org';

var mailgun = require('mailgun-js')({
    apiKey : mailgunAPIkey,
    domain : mailgunDomain
});



app.post('/sendmail', function(req, res, next) {
    var data = "";
    var email = "";
    var azienda = "";
    var indirizzoFatturazione = "";
    var piva = "";
    var phone = "";

    email = req.query.mail;
    azienda = req.query.companyName;
    indirizzoFatturazione = req.query.invoiceAddress;
    piva = req.query.VATcode;
    phone = req.query.phone;
    launchmail(email, azienda, indirizzoFatturazione, piva, phone, res);

    console.log('body: ', req.query);
});

//http://code.tutsplus.com/tutorials/how-to-scrape-web-pages-with-nodejs-and-jquery--net-22478
app.get('/generate-products-snapshots', function(req, res) {
    renderer.render({
        url: "http://mb-fix.it/",
        scripts: ['http://code.jquery.com/jquery-1.6.min.js'],
        pollSelector: 'body',
        pollSelectorMS: 100,
        timeoutMS: 16000,
        features: {
            FetchExternalResources : ["link", "script"],
            ProcessExternalResources: true
        },
        done: function(errors, window) {
            //Use jQuery just as in any regular HTML page
            var $ = window.jQuery,
            $links = $('a'),
            linkArray = [];

            var message = "";

            // look for links
            $links.each(function(i, item) {
            	if ($(this).attr('href').indexOf('#!') >= 0) {
                 message += $(this).attr('href') + "<br/>";
                 linkArray.push($(this).attr('href'));
             }
         });
            // now print array
            if (linkArray.length > 0) {
                var file = filesys.createWriteStream(__dirname + '/Gruntfile.generator.js');
                file.on('error', function(err) {
                    console.log(err);
                });
                file.write('[');
                linkArray.forEach(function(v) {
                    file.write('"' + v + '", \n');
                });
                file.write(']')
                file.end();
            }
            if (errors) {
            	console.log(errors);
            }

            // END RESPONSE
            res.end('<html><head></head><body>Done at <strong>' + __dirname + '/Gruntfile.generator.js</strong><br/><br/>' + message + '</body></html>');
        }
    });
});

// all environments
app.set('port', process.env.PORT || 19017);



app.use(function(req, res, next) {

    console.log("DEBUG\t --- Requested original url ", req.url);
    if (req.originalUrl.indexOf('_escaped_fragment_') >= 0 && req.url.match(/(jpg|jpeg|png|css|js|woff|eot|ttf)/g) === null) {
        console.log('----> RENDERING FOR BOT');
        getPagesForBot(req, res, next);
    } else {
        console.log('----> RENDERING FOR HUMAN')
        if (app.get('env') == 'dist') {
            app.use(express.static(path.join(__dirname, './dist')));
            app.use('./dist/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        } else if (app.get('env') == 'prod') {
            app.use(express.static(path.join(__dirname, './prod')));
            app.use('./prod/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        } else {
            app.use(express.static(path.join(__dirname, './prod')));
            app.use('./prod/snapshots', express.static(path.join(__dirname, '/snapshots')));
            next();
        }
    }
});




http.createServer(app).listen(app.get('port'), function() {
    console.log("Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});

// app.listen(app.get('port'));

// function getPagesForBot(request, response, next) {
//     // console.log("DEBUG\t --- Requested url ", request.url)
//     var full_path = thePath(request, response);

//     console.log('DEBUG SEO\t --- requested url', request.url)

//     if (request.url.indexOf('index') >= 0 || request.url === '/') {
//     	console.log('DEBUG SEO\t --- requested index')
//     	full_path += '/pages/MB___.html';
//     } else if (request.url.indexOf('/catalogue') >= 0) {
//     	console.log('DEBUG SEO\t --- requested catalogue')
//     	full_path = full_path.replace('catalogue', 'MB___catalogue');
//     } else if (request.url.indexOf('/category') >= 0) {
//     	console.log('DEBUG SEO\t --- requested category')
//     	full_path = full_path.replace('category', 'MB___category');
//     } else if (request.url.indexOf('/categories') >= 0) {
//     	console.log('DEBUG SEO\t --- requested categories')
//     	full_path = full_path.replace('categories', 'MB___categories');
//     } else if (request.url.indexOf('/certificates') >= 0) {
//     	console.log('DEBUG SEO\t --- requested certificates')
//     	full_path = full_path.replace('certificates', 'MB___certificates');
//     } else if (request.url.indexOf('/products') >= 0) {
//     	console.log('DEBUG SEO\t --- requested products')
//     	full_path = full_path.replace('products', 'MB___products');
//     } else if (request.url.indexOf('/services') >= 0) {
//     	console.log('DEBUG SEO\t --- requested services')
//     	full_path = full_path.replace('services', 'MB___services');
//     } else if (request.url.indexOf('/vdi-matrix') >= 0) {
//     	console.log('DEBUG SEO\t --- requested vdi-matrix')
//     	full_path = full_path.replace('contacts', 'MB___vdi-matrix');
//     } else if (request.url.indexOf('/vdi') >= 0) {
//     	console.log('DEBUG SEO\t --- requested vdi')
//     	full_path = full_path.replace('vdi', 'MB___vdi');
//     } else if (request.url.indexOf('/video') >= 0) {
//     	console.log('DEBUG SEO\t --- requested video')
//     	full_path = full_path.replace('video', 'MB___video');
//     } else if (request.url.indexOf('/vision') >= 0) {
//     	console.log('DEBUG SEO\t --- requested vision')
//     	full_path = full_path.replace('vision', 'MB___vision');
//     } else {
//     	console.log('DEBUG SEO\t --- Requested component ', full_path)
//     }
//     console.log("DEBUG SEO\t --- Rendered url ", full_path)
//     getFile(request, response, next, full_path)

// }

function launchmail(email, azienda, indirizzoFatturazione, piva, phone, res) {
    console.log('inside launchmail function');
    var mbFixEmail = 'noreply@mb-fix.it';
    var data = {};
    var sentFrom = "";

    var body = "<h2>Richiesta registrazione nuovo utente</h2>";
    body += "<p><strong>" + azienda + "</strong> ha richiesto di poter accedere all'<a href='http://localhost:9000/#!/login'>area clienti</a></p>";
    body += "<h3>Dati del cliente:</h3>";
    body += "<p><strong>Azienda</strong> " + azienda + "</p>";
    body += "<p><strong>Email:</strong> " + email + "</p>";
    body += "<p><strong>Indirizzo di fatturazione:</strong> " + indirizzoFatturazione + "</p>";
    body += "<p><strong>Partita Iva:</strong> " + piva + "</p>";
    body += "<p><strong>Telefono:</strong> " + phone + "</p>";

    var text = azienda + " ha richiesto di poter accedere all'area clienti";

    data = {
        from : 'MB-FIX - WEB CONTACT <noreply@mb-fix.it>',
        to : 'filippo.tasca@crispybacon.it',
        subject : 'Richiesta Registrazione Nuovo Cliente',
        text : text,
        html : body
    };

    mailgun.messages().send(data, function(error, body) {
        console.log(data);
        if (error) {
            console.log(error);
            res.render('error', {
                error : error
            });
            res.set("Access-Control-Allow-Origin", "*");
            res.set({
                "Content-Type" : "text/plain; charset=UTF-8"
            });
            res.status(500).send();
        } else {
            console.log(body);
            console.log('Message sent ', body.response);
            res.set("Access-Control-Allow-Origin", "*");
            res.set({
                "Content-Type" : "text/plain; charset=UTF-8"
            });
            res.status(200).send();
        }
    });
}



function getPagesForBot(request, response, next) {
    var full_path = thePath(request, response);

    // SANITIZE PATH
    console.log('DEBUG SEO\t --- requested url', full_path)
    full_path = full_path.replace('#!/', '');

    if (request.url.indexOf('/catalogue') >= 0) {
        full_path = full_path.replace('catalogue', 'snapshots/catalogue/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/category') >= 0) {
        full_path = full_path.replace('category', 'snapshots/category/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/contacts') >= 0) {
        full_path = full_path.replace('contacts', 'snapshots/contacts/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/categories') >= 0) {
        full_path = full_path.replace('categories', 'snapshots/categories/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/certificates') >= 0) {
        console.log('DEBUG SEO\t --- requested certificates')
        full_path = full_path.replace('certificates', 'snapshots/certificates/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/products') >= 0) {
        console.log('DEBUG SEO\t --- requested products')
        full_path = full_path.replace('products', 'snapshots/products/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/services') >= 0) {
        console.log('DEBUG SEO\t --- requested services')
        full_path = full_path.replace('services', 'snapshots/services/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vdi-matrix') >= 0) {
        console.log('DEBUG SEO\t --- requested vdi-matrix')
        full_path = full_path.replace('vdi-matrix', 'snapshots/vdi-matrix/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vdi') >= 0) {
        full_path = full_path.replace('vdi', 'snapshots/vdi/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/video') >= 0) {
        console.log('DEBUG SEO\t --- requested video')
        full_path = full_path.replace('video', 'snapshots/video/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url.indexOf('/vision') >= 0) {
        full_path = full_path.replace('vision', 'snapshots/vision/index.html');
        full_path = full_path.replace('pages/', '');
    } else if (request.url === '/') {
        console.log('DEBUG SEO\t --- Requested component ', full_path)
        full_path += 'snapshots/index.html';
    }
    console.log("DEBUG SEO\t --- Rendered url ", full_path)
    getFile(request, response, next, full_path)
}


function getFile(request, response, next, path) {
    var n = path.indexOf('?');
    var full_path = path.substring(0, n != -1 ? n : path.length);

    filesys.readFile(path, 'utf8', function(err, data) {
        if (err) {
            console.log('DEBUG ERROR\t --- ', err);
            response.sendStatus(500);
            response.end();
        } else {
            filesys.exists(full_path, function(exists) {
                if (!exists) {
                    console.log('DEBUG NOT FOUND\t --- file does not exist');
                    response.sendStatus(404);
                    response.end();
                } else {
                    var file = data;
                    response.status(200);
                    response.send(file);
                    response.end();
                }
            })
        }
    })
}

function thePath(request, response) {
    console.log("PATH BEFORE\t --- original full_path " + request.url + " with method " + request.method);
    request.url = request.url.replace('?_escaped_fragment_=/', '');
    var newpath = url.parse(request.url).pathname;
    var folder = app.get('env') === 'dist' ? '/dist' : '/prod';
    newpath = folder + newpath;
    var full_path = path.join(process.cwd(), newpath);
    console.log("PATH AFTER\t --- Created full_path " + full_path + " with method " + request.method);

    return full_path;
}