var express = require('express'),
http = require('http'),
path = require('path'),
url = require("url"),
filesys = require("fs"),
htmlSnapshots = require('html-snapshots'),
os = require('os'),
app = express();


setInterval(function() {

    var basepath = "http://localhost:19017/";
    var basepathReplace = "http://mb-fix.it/"
    var pages = [basepath + "#!/", basepath + '#!/vision', basepath + '#!/contacts', basepath + '#!/contacts', basepath + '#!/services', basepath + '#!/certificates', basepath + '#!/products', basepath + '#!/category', basepath + '#!/categories', basepath + '#!/vdi', basepath + '#!/vdi-matrix', basepath + '#!/video', basepath + '#!/catalogue'];
    var outputPathGenerator = function(pages) {
        var ret = {}
        for (var i = 0; i < pages.length; i++) {
            ret['' + pages[i]] = pages[i].replace('http://localhost:19017/#!', '')
        }
        console.log('DIRNAME ', __dirname)
        return ret;
    }
    var result = htmlSnapshots.run({
        hostname: "localhost",
        port: '19017',
        outputDir: path.join(__dirname, "./dist/snapshots"),
        outputDirClean: true,
        input: 'array',
        source: pages,
        outputPath: outputPathGenerator(pages),
        selector: {
            "catalogue.html": "#catalogue-page",
            "categories.html": "#types",
            "category.html": "#category",
            "contacts.html": "#contacts",
            "certificates.html": "#certificates-page",
            "contacts.html": "#contacts-page",
            "home.html": "#home",
            "product.html": "#product",
            "services.html": "#services-page",
            "vdi-matrix.html": "#vdi-matrix-page",
            "vdi.html": "#vdi-page",
            "video.html": "#video-page",
            "vision.html": "#vision-page",
        }
    }, function(err, snapshotsCompleted) {
        if (!err) {
            snapshotsCompleted.forEach(function(snapshotFile) {
                filesys.readFile(snapshotFile, 'utf8', function(err, data) {
                        // SANITIZE SOURCES
                        if (err) {
                            return console.log(err);
                        }
                        var result = data.replace(/href="styles/g, 'href="' + basepathReplace + 'styles'); // absolute path fot styles
                        result = result.replace(/href="\/#!/g, 'href="'); // absolute path fot styles
                        result = result.replace(/src="content/g, 'src="' + basepathReplace + 'content'); // absolute path fot content
                        result = result.replace(/src="scripts/g, 'src="' + basepathReplace + 'scripts'); // absolute path fot scripts
                        result = result.replace(/ ng-app="app" /g, ' '); // disable app
                        result = result.replace("</body>", "<script type='text/javascript'>jQuery('.navbar-nav').find('li').each(function() {var $this = jQuery(this); $this.hover(function(){$this.addClass('open')}, function() {$this.removeClass('open')})})</script></body>"); // dropdown menu
                        // result = result.replace(/localhost:19017/g, 'mb-fix.it');

                        filesys.writeFile(snapshotFile, result, 'utf8', function(err) {
                            if (err) return console.log(err);
                        });
                    });
                snapshotFile = snapshotFile.replace('#!', 'snapshots')
                console.log("SNAPSHOT --- ", snapshotFile)
            });
        }
    });


}, 60000) // EVERY 1 minute
