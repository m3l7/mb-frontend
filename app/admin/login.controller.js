(function(){
	angular
		.module('app')
		.controller('Login',Login);

		Login.$inject = ['$scope','$timeout','$location', 'security'];

		function Login($scope,$timeout,$location, security){

			$scope.login = login;

			function login(){
                var user = $scope.user;
                var pass = $scope.password;
                
                security.login(user,pass,function(){
                    if (security.isAuthenticated){
                        $location.path('/admin');
                    }
                    else{
                        $scope.login_error = 'Nome utente o password non validi';
                        $timeout(function(){
                            $scope.login_error = '';
                        },5000)
                    }
                });

            }

		}
})();
