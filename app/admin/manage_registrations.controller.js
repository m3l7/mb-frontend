(function(){
    angular
        .module('app')
        .controller('ManageRegistrations',ManageRegistrations);

        ManageRegistrations.$inject = ['$scope', '$filter', 'config', 'UserModel', 'ngTableParams'];

        function ManageRegistrations($scope, $filter, config, UserModel, ngTableParams){

            $scope.baseUrlAssets = config.API.baseUrlAssets;
            $scope.activate = activate;
            $scope.deactivate = deactivate;
            $scope.remove = remove;

            function activate(user){
                if (!!user){
                    user.activated = true;
                    UserModel.save(user);
                }
            }
            function deactivate(user){
                if (!!user){
                    user.activated = false;
                    UserModel.save(user);
                }
            }
            function remove(user){
                if (!!user){
                    UserModel.destroy(user)
                    .then(function(){
                        $scope.tableParams.reload();
                    })
                }
            }


            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 25,          // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                },
                filter:{
                    companyName: '',
                    VATcode: '',
                }
            },
            {
                // counts:[],
                total: 100,
                getData: function($defer, params) {
                    UserModel.findAll({where:{
                            role: 'client'
                        }}).then(function(data){
                        var orderedData = params.sorting() ?
                            $filter('orderBy')(data, params.orderBy()) :
                            data;
                        var orderedData = params.filter() ?
                           $filter('filter')(orderedData, params.filter()) :
                           orderedData;
                        params.total(orderedData.length);
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    });
                }
            });

        }
})();