(function(){
	angular
  .module('app')
  .controller('Register',Register);

  Register.$inject = ['$rootScope', '$scope', '$http', '$timeout','$location', '$translate', 'security', 'config'];

  function Register($rootScope, $scope, $http, $timeout,$location, $translate, security, config) {

    var host = window.location.hostname;
    if (host.indexOf('localhost') >= 0) {
      host = 'http://localhost';
    } else {
      if (host.indexOf('.com') >= 0) {
        host = 'http://mb-fix.com';
      } else {
        host = 'http://mb-fix.it';
      }
    }

   $scope.registerFields = {};
   $scope.baseUrlAssets = config.API.baseUrlAssets;
   $scope.fileChange = fileChange;
   $scope.register = register;
   $scope.uploadComplete = false;

   function fileChange(files,product){
    if (files.length){
      $scope.file = files[0];
    } else {
      $scope.file = {};
    }
  }

  function register() {
    $scope.hideForm = true;
    $translate('_REGISTRAZIONE_IN_ELABORAZIONE').then(function(t){ 
      showAlert(t,'primary');
      console.log($scope.file);
      if (!$scope.file || typeof $scope.file === 'undefined') {
        $scope.file = {};
        console.log("$scope.file was undefined, now its: ", $scope.file);
      }
      security.register($scope.registerFields,$scope.file,function(err,response){
        if (!!err){
          showAlert('Error: '+err,'danger');
        }
        else {
          $http({
            url: host + ':19017/sendmail',
            method: 'POST',
            data: $scope.registerFields,
          }).then(function success(response) {
            $scope.uploadComplete = true;
            $scope.uploadCompleteText = response.data;
            $translate('_REGISTRAZIONE_IN_CONVALIDA').then(function(t){ 
            showAlert(t,'success');
          });
          }, function error(response) {
            showAlert('Error: '+err,'danger');
            $scope.uploadComplete = true;
            $scope.uploadCompleteText = response.data;
          });
        }
      });
    });
  }

  function showAlert(text,alertClass,timeout) {
    if (!$scope.alert) {
      $scope.alert = {};
    }
    $scope.alert.enabled = true;
    $scope.alert.class = alertClass;
    $scope.alert.text = text;
    $scope.$watch('progressBar', function() {
      if ($rootScope.progressBar === 100) {
        // DO SOMETHING
      }
    });
  }
}
})();
