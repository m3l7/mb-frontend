(function(){
	angular
		.module('app')
		.controller('AdminHeader',AdminHeader);

		AdminHeader.$inject = ['$scope', '$location'];

		function AdminHeader($scope, $location){

			$scope.isActive = isActive;
			

			function isActive(path,fixed) {
              if (fixed==true) return path === $location.path();
              else if ($location.path().substr(0, path.length) == path) return true;
              else return false;
            };
		}
})();