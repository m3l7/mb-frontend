(function(){
    angular
        .module('app')
        .controller('ManageSubcat',ManageSubcat);

        ManageSubcat.$inject = ['$scope', '$filter', 'config', 'ngTableParams', 'SubcategoryModel', 'CategoryModel', 'ProductModel'];

        function ManageSubcat($scope, $filter, config, ngTableParams, SubcategoryModel, CategoryModel, ProductModel){

            $scope.baseUrlAssets = config.API.baseUrlAssets;

            $scope.saveSubcat = saveSubcat;
            $scope.newSubcat = newSubcat;
            $scope.removeSubcat = removeSubcat;

            initialize();

            $scope.tableParamsSubcat = new ngTableParams({
                page: 1,            // show first page
                count: 100,          // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            },
            {
                counts:[],
                getData: function($defer, params) {
                    // var data = $scope.subcategories;
                    SubcategoryModel.findAll().then(function(data){
                        var data = $scope.subcategories
                        var orderedData = params.sorting() ?
                                $filter('orderBy')(data, params.orderBy()) :
                                data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    });
                }
            });

            function initialize(){
                CategoryModel.findAll();
                CategoryModel.bindAll($scope,'categories',{},function(err,items){
                    if (!!items && items.length){
                        var toDelete = [];
                        items.forEach(function(item){
                            if (!!item.belongsTo) toDelete.push(item.belongsTo);
                        });
                        items.forEach(function(item,i){
                            if (toDelete.indexOf(item.id)!=-1) items.splice(i,1); 
                        })
                    }
                });
                SubcategoryModel.findAll()
                .then(function(subcats){
                    subcats.forEach(function(subcat){
                        SubcategoryModel.loadRelations(subcat,['category']);
                    })
                })
                SubcategoryModel.bindAll($scope,'subcategories');
            }


            //MODELS FUNCTIONS
            function saveSubcat(subcat){
                if (!!subcat){
                    //strip cat population
                    delete subcat.category;
                    delete subcat.products;
                    SubcategoryModel.save(subcat)
                    .then(function(s){
                        console.log(s);
                        SubcategoryModel.loadRelations(s,['category']);
                    })
                }
            }

            function removeSubcat(subcat){
                if (!!subcat){
                    SubcategoryModel.destroy(subcat)
                    .then(function(){
                        $scope.tableParamsSubcat.reload();
                    })
                }
            }
            function newSubcat(){
                SubcategoryModel.create({})
                .then(function(){
                    $scope.tableParamsSubcat.reload();
                })
            }

        }
})();
