(function(){
    angular
        .module('app')
        .controller('ManageProducts',ManageProducts);

        ManageProducts.$inject = ['$scope', '$filter', 'config', 'ProductModel', 'ngTableParams', 'SubcategoryModel', '$upload', 'CategoryModel', 'SlideshowModel', 'langUtils'];

        function ManageProducts($scope, $filter, config, ProductModel, ngTableParams, SubcategoryModel, $upload, CategoryModel, SlideshowModel, langUtils){

            $scope.baseUrlAssets = config.API.baseUrlAssets;

            $scope.saveProduct = saveProduct;
            $scope.newProduct = newProduct;
            $scope.removeProduct = removeProduct;

            $scope.removeFile = removeFile;
            $scope.pdfChange = pdfChange;

            $scope.fileChange = fileChange;

            $scope.adminLang = langUtils.currentLang;
            $scope.langs = config.lang.langs;
            $scope.changeAdminLang = changeAdminLang;

            initialize();

            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 200,          // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                },
                filter:{
                    name: ''
                }
            },
            {
                counts:[],
                getData: function($defer, params) {
                    ProductModel.findAll().then(function(data){
                        data.forEach(function(product){
                            if (!product.name) {
                                //name must be initialized for ng-table support
                                product.name = '';
                            }
                        })
                        var orderedData = params.sorting() ?
                            $filter('orderBy')(data, params.orderBy()) :
                            data;
                        var orderedData = params.filter() ?
                           $filter('filter')(orderedData, params.filter()) :
                           orderedData;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    });
                }
            });

            function initialize(){
                CategoryModel.findAll();
                CategoryModel.bindAll($scope,'categories');
                SubcategoryModel.ejectAll();
                SubcategoryModel.findAll()
                .then(function(subcats){
                    subcats.forEach(function(subcat){
                        SubcategoryModel.loadRelations(subcat,['category'])
                        .then(function(sc){
                            sc.full_label = sc.category.name.en + ' - ' + sc.name.en; //save a cat + subcat for use in option
                        })
                    })
                })
                SubcategoryModel.bindAll($scope,'subcategories');
                ProductModel.findAll()
                .then(function(products){
                    products.forEach(function(product){
                        ProductModel.loadRelations(product,['subcategory']);
                    })
                })
            	ProductModel.bindAll($scope,'products');
            }

            function changeAdminLang(lang){
                if (!!lang) $scope.adminLang = lang;
            }

            //MODELS FUNCTIONS
            function saveProduct(product){
                if (!!product){
                    //strip subcat population
                    delete product.subcategory;
                    ProductModel.save(product)
                    .then(function(p){
                        ProductModel.loadRelations(p,['subcategory']);
                    })
                }
            }
            function newProduct(){
                ProductModel.create({
                    description: {},
                    name: ''
                })
                .then(function(){
                    $scope.tableParams.reload();
                })
            }

            function removeProduct(product){
                if (!!product){
                    ProductModel.destroy(product)
                    .then(function(){
                        SlideshowModel.ejectAll();
                        SlideshowModel.findAll({}, {bypassCache: true});
                        $scope.tableParams.reload();
                    })
                }
            }


            function removeFile(product,pdf){
                if (!!product){
                    var i = product.pdf.indexOf(pdf);
                    if (i!=-1){
                        product.pdf.splice(i,1);
                        delete product.subcategory;
                        ProductModel.save(product.id)
                        .then(function(pr){
                            ProductModel.loadRelations(pr,['subcategory']);
                        })
                    }
                }
            }
            function pdfChange(files,product){
                if ((files.length) && (!!product)){
                    var file = files[0];

                    $upload.upload({
                      url: config.API.baseUrl+'product/'+product.id+'/pdf/'+$scope.adminLang, // upload.php script, node.js route, or servlet url
                      method: 'POST',
                      headers: {'Authorization': 'bearer '+localStorage.getItem("token")}, // only for html5
                      file: file, // single file or a list of files. list is only for html5,
                    }).progress(function(evt) {
                                    // console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        if (status==200){
                            if (!product.pdf) product.pdf = [];
                            product.pdf.push(data);
                        }
                    });

                }
            }

            function fileChange(files,product){
                if ((files.length) && (!!product)){
                    var file = files[0];

                    $upload.upload({
                      url: config.API.baseUrl+'product/'+product.id+'/photo', // upload.php script, node.js route, or servlet url
                      method: 'POST',
                      headers: {'Authorization': 'bearer '+localStorage.getItem("token")}, // only for html5
                      file: file, // single file or a list of files. list is only for html5,
                    }).progress(function(evt) {
                                    // console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        if (status==200) product.image = data;
                    });

                }
            }
        }
})();
