(function(){
    angular
        .module('app')
        .controller('Slideshow',Slideshow);

        Slideshow.$inject = ['$scope', '$upload', 'config', 'SlideshowModel', 'ProductModel', 'SubcategoryModel', 'langUtils'];

        function Slideshow($scope, $upload, config, SlideshowModel, ProductModel, SubcategoryModel, langUtils){

            $scope.baseUrlAssets = config.API.baseUrlAssets;
            $scope.saveSlide = saveSlide;
            $scope.newSlide = newSlide;
            $scope.removeSlide = removeSlide;
            $scope.fileChange = fileChange;
            $scope.deleteImage = deleteImage;
            $scope.adminLang = langUtils.currentLang;
            $scope.langs = config.lang.langs;
            $scope.changeAdminLang = changeAdminLang;
            $scope.sortUp = sortUp;
            $scope.sortDown = sortDown;

            initialize();

            function initialize(){
                SlideshowModel.findAll()
                .then(function(slides){
                    slides.forEach(function(slide){
                        SlideshowModel.loadRelations(slide,['product']);
                    });
                });
                SlideshowModel.bindAll($scope,'slides');
                ProductModel.findAll();
            	ProductModel.bindAll($scope,'products');                
            }

            function sortUp(slide){
                if ((!!slide) && (slide.position>1)){
                    slide.position--;
                    $scope.slides.forEach(function(p){
                        if (p.id==slide.id) return;
                        if (p.position==slide.position){
                            p.position++;
                            SlideshowModel.save(p);
                        }
                    });
                    SlideshowModel.save(slide);
                }
            }
            function sortDown(slide){
                if ((!!slide) && (!isBottomSlide(slide))){
                    slide.position++;
                    $scope.slides.forEach(function(p){
                        if (p.id==slide.id) return;
                        if (p.position==slide.position){
                            p.position--;
                            SlideshowModel.save(p);
                        }
                    });
                    SlideshowModel.save(slide);
                }
            }

            function bottomSlide(){
                var bottom = {position:0};
                $scope.slides.forEach(function(p){
                    if (p.position>bottom.position) bottom = p;
                })
                return bottom;
            }

            function isBottomSlide(slide){
                if (!!slide) return (slide.id==bottomSlide().id);
                else return false
            }

            function changeAdminLang(lang){
                if (!!lang) $scope.adminLang = lang;    
            }

            function saveSlide(slide){
                if (!!slide){
                    //strip product populated fields
                    delete slide.product;
                    SlideshowModel.save(slide)
                    .then(function(s){
                        SlideshowModel.loadRelations(s,['product']);
                    })
                }
            }
            function newSlide(){
                SlideshowModel.create({
                    position: bottomSlide().position+1
                })
            }


            function removeSlide(slide){
                if (!!slide) SlideshowModel.destroy(slide);
            }


            function fileChange(files,slide){
                if ((files.length) && (!!slide)){
                    var file = files[0];

                    $upload.upload({
                      url: config.API.baseUrl+'slide/'+slide.id+'/photo', // upload.php script, node.js route, or servlet url
                      method: 'POST',
                      headers: {'Authorization': 'bearer '+localStorage.getItem("token")}, // only for html5
                      file: file, // single file or a list of files. list is only for html5,
                    }).progress(function(evt) {
                                    // console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        if (status==200) slide.image = data;
                    });

                }
            }
            function deleteImage(slide){
                slide.image = '';
                SlideshowModel.save(slide);
            }
        }
})();