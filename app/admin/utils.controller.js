(function(){
	angular
		.module('app')
		.controller('AdminUtils',AdminUtils);

		AdminUtils.$inject = ['$scope','InfoModel'];

		function AdminUtils($scope,InfoModel){

			$scope.saveInfo = saveInfo;

			initialize();

			function initialize(){
				InfoModel.findAll();
				InfoModel.bindAll($scope,'info');
			}

			function saveInfo(){
				InfoModel.save($scope.info[0]);
			}

		}
})();