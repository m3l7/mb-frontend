(function(){
    angular
        .module('app')
        .factory('TwitModel',TwitModel);

        TwitModel.$inject = ['DS'];

        function TwitModel(DS){

            return DS.defineResource({
                name: 'twit',
            });
        }
})();