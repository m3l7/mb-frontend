(function(){
    angular
        .module('app')
        .factory('CategoryModel',CategoryModel);

        CategoryModel.$inject = ['DS'];

        function CategoryModel(DS){
            console.log("DS: ", DS);
            return DS.defineResource({
                name: 'category',
                relations:{
                    hasMany:{
                        subcategory:{
                            localField: 'subcategories',
                            foreignKey: 'categoryId'
                        }
                    }

                }
            });
        }
})();