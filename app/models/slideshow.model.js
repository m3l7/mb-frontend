(function(){
    angular
        .module('app')
        .factory('SlideshowModel',SlideshowModel);

        SlideshowModel.$inject = ['DS'];

        function SlideshowModel(DS){

            return DS.defineResource({
                name: 'slideshow',
                relations:{
                    belongsTo:{
                        product:{
                            localField: 'product',
                            localKey: 'productId',
                        }
                    },
                }
            });
        }
})();