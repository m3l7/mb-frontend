(function(){
    angular
        .module('app')
        .factory('UserModel',UserModel);

        UserModel.$inject = ['DS'];

        function UserModel(DS){

            return DS.defineResource({
                name: 'user',
            });
        }
})();