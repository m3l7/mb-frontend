(function(){
    angular
        .module('app')
        .factory('InfoModel',InfoModel);

        InfoModel.$inject = ['DS'];

        function InfoModel(DS){

            return DS.defineResource({
                name: 'info',
            });
        }
})();