(function(){
    angular
        .module('app')
        .factory('ProductModel',ProductModel);

        ProductModel.$inject = ['DS'];

        function ProductModel(DS){

            return DS.defineResource({
                name: 'product',
                relations:{
                    belongsTo:{
                        subcategory:{
                            localField: 'subcategory',
                            localKey: 'subcategoryId',
                        }
                    },
                }
            });
        }
})();