(function(){
    angular
        .module('app')
        .factory('VimeoModel',VimeoModel);

        VimeoModel.$inject = ['DS'];

        function VimeoModel(DS){

            return DS.defineResource({
                name: 'vimeo',
                endpoint: 'mbfix/videos.json',
                baseUrl: 'https://vimeo.com/api/v2/',
            });
        }
})();