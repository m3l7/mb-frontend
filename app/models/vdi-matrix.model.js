(function(){
    angular
    .module('app')
    .factory('VDIMatrixModel',VDIMatrixModel);

    VDIMatrixModel.$inject = ['VDI'];

    function VDIMatrixModel(VDI){
        return VDI.searchResults();
    }

})();