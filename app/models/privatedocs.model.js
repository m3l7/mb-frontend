(function(){
    angular
        .module('app')
        .factory('PrivatedocModel',PrivatedocModel);

        PrivatedocModel.$inject = ['DS'];

        function PrivatedocModel(DS){

            return DS.defineResource({
                name: 'privatedoc',
            });
        }
})();