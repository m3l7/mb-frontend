(function(){
    angular
        .module('app')
        .factory('PrivatevideoModel',PrivatevideoModel);

        PrivatevideoModel.$inject = ['DS'];

        function PrivatevideoModel(DS){

            return DS.defineResource({
                name: 'privatevideo',
            });
        }
})();