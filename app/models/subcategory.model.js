(function(){
    angular
        .module('app')
        .factory('SubcategoryModel',SubcategoryModel);

        SubcategoryModel.$inject = ['DS'];

        function SubcategoryModel(DS){

            return DS.defineResource({
                name: 'subcategory',
                relations:{
                    belongsTo:{
                        category:{
                            localField: 'category',
                            localKey: 'categoryId',
                        }
                    },
                    hasMany:{
                        product:{
                            localField: 'products',
                            foreignKey: 'subcategoryId'
                        }
                    }

                }
            });
        }
})();