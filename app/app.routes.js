(function(){
  angular
    .module('app')
    .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'pages/home.html',
          controller: 'Home',
        })
        .when('/category', {
          templateUrl: 'pages/categories.html',
          controller: 'Categories',
        })
        .when('/products/:productId', {
          templateUrl: 'pages/product.html',
          controller: 'Product',
        })
        .when('/products', {
          templateUrl: 'pages/categories.html',
          controller: 'Categories',
        })
        .when('/category/:categoryId', {
          templateUrl: 'pages/category.html',
          controller: 'Category',
        })
        .when('/admin', {
          //this is only a dummy url, see auth.routes.js for routing
          data:{
            roles: ['admin','client']
          }
        })
        .when('/admin/registrations', {
          templateUrl: 'admin/manage_registrations.html',
          controller: 'ManageRegistrations',
          data:{
            roles: ['admin']
          }
        })
        .when('/private/docs', {
          templateUrl: 'private/private.docs.html',
          controller: 'PrivateDocs',
          data:{
            roles: ['admin','client']
          }
        })
        .when('/private/video', {
          templateUrl: 'private/private.video.html',
          controller: 'PrivateVideo',
          data:{
            roles: ['admin','client']
          }
        })
        .when('/admin/slideshow', {
          templateUrl: 'admin/slideshow.html',
          controller: 'Slideshow',
          data:{
            roles: ['admin']
          }
        })
        .when('/admin/products', {
          templateUrl: 'admin/manage_products.html',
          controller: 'ManageProducts',
          data:{
            roles: ['admin']
          }
        })
        .when('/admin/subcat', {
          templateUrl: 'admin/manage_subcat.html',
          controller: 'ManageSubcat',
          data:{
            roles: ['admin']
          }
        })
        .when('/admin/utils', {
          templateUrl: 'admin/utils.html',
          controller: 'AdminUtils',
          data:{
            roles: ['admin']
          }
        })
        .when('/login', {
          templateUrl: 'admin/login.html',
          controller: 'Login',
        })
        .when('/register', {
          templateUrl: 'admin/register.html',
          controller: 'Register',
        })
        .when('/contacts', {
          templateUrl: 'pages/contacts.html',
          controller: 'Contacts',
        })
        .when('/vdi', {
          templateUrl: 'pages/vdi.html',
          controller: 'Vdi',
        })
        .when('/vdi-matrix', {
          templateUrl : "pages/vdi-matrix.html",
          controller : "VdiMatrix"
        })
        .when('/vision', {
          templateUrl: 'pages/vision.html',
          controller: 'Vision',
        })
        .when('/catalogue', {
          templateUrl: 'pages/catalogue.html',
          controller: 'Catalogue',
        })
        .when('/services', {
          templateUrl: 'pages/services.html',
        })
        .when('/certificates', {
          templateUrl: 'pages/certificates.html',
          controller: 'Certificates',
        })
        .when('/video/:videoId', {
          templateUrl: 'pages/video.html',
          controller: 'Video',
        })
        .when('/video', {
          templateUrl: 'pages/video.html',
          controller: 'Video',
        })
        .otherwise({
          redirectTo: '/'
        });
    });
})();
