(function(){
  var config = {
    logging:{
      http: true
    },
    API:{
      baseUrl: 'http://mb-fix.it:14040/apiv1/',
      baseUrlAssets: 'http://mb-fix.it:14040/store/',
          // baseUrl: 'http://localhost:1337/apiv1/',
          // baseUrlAssets : 'http://localhost:1337/store/',
          baseUrlPrivate: 'http://mb-fix.it:14040/privatestore/',
          idAttribute: 'id',
        },
        lang:{
          // langs: ['en'],
          langs: ['it','en','de','fr'],
          defaultLang: 'en',
          //langDetect: false // use default lang of browser
          langDetect: true
        },
        url:{
          prefix: '/#!'
        },
        homeSlideshow:{
          interval: 10000
        },
        homeAnimation:{
          restartInterval: 45000
        }
      }

      angular
      .module('app')
      .constant('config',config)
      .config(['$locationProvider', '$httpProvider', function($location, $httpProvider) {
        $location.hashPrefix('!');
        $location.html5Mode(false);
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        // $httpProvider.defaults.headers.common['Access-Control-Allow-‌​Headers'] = '*';
        // $httpProvider.defaults.headers.common['Access-Control-Allow-‌​Origin'] = '*';
        // $httpProvider.defaults.headers.common['Access-Control-Allow-‌​Methods'] = '*';
      }]);
    })();
