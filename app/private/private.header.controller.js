(function(){
	angular
		.module('app')
		.controller('PrivateHeader',PrivateHeader);

		PrivateHeader.$inject = ['$scope', '$location'];

		function PrivateHeader($scope, $location){

			$scope.isActive = isActive;
			

			function isActive(path,fixed) {
              if (fixed==true) return path === $location.path();
              else if ($location.path().substr(0, path.length) == path) return true;
              else return false;
            };
		}
})();