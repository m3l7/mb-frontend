(function(){
	angular
		.module('app')
		.controller('PrivateDocs',PrivateDocs);

		PrivateDocs.$inject = ['$scope', '$http', 'PrivatedocModel', 'config'];

		function PrivateDocs($scope, $http, PrivatedocModel, config){

			$scope.baseUrlPrivate = config.API.baseUrlPrivate;

			initialize();

			function initialize(){
				PrivatedocModel.findAll();
				PrivatedocModel.bindAll($scope,'docs');
			}
		}
})();