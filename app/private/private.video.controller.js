(function(){
	angular
		.module('app')
		.controller('PrivateVideo',PrivateVideo);

		PrivateVideo.$inject = ['$scope', '$http', 'PrivatevideoModel', 'config'];

		function PrivateVideo($scope, $http, PrivatevideoModel, config){

			$scope.baseUrlPrivate = config.API.baseUrlPrivate;

			initialize();

			function initialize(){
				PrivatevideoModel.findAll();
				PrivatevideoModel.bindAll($scope,'videos');
			}
		}
})();