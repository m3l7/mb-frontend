(function(){

	var categories = [
		{
			id: 1,
			name:{
				it: "Radiatori con pannello d'acciao",
				en: "Steel panel radiators"
			},
			image: "categories/STEEL PANEL RADIATOR.png"
		}

	]    

    var testData = {
        categories: categories
    }

    angular
        .module('app')
        .constant('testData',testData);
})();
