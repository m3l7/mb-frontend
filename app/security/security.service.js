(function(){
  angular
  .module('app')
  .factory('security',phzSecurity)
  .controller('UploadProgress', regController);

  regController.$inject = ['$rootScope', '$scope', 'security'];


  function regController($rootScope, $scope, security) {
  };

  phzSecurity.$inject = ['$rootScope', '$http','$timeout','$location','config','$upload'];

  function phzSecurity($rootScope, $http, $timeout,$location,config, $upload){

    $rootScope.progressBar = 0;

    //example of when you may want to notify observers
    // this.progress = someNgResource.query().$then(function(){
    // });

    var service = {

      logout: logout,
      login: login,
      register: register,
      isAuthenticated: false,
      isAuthorized: isAuthorized,
      getRole: getRole,

    };

    if (localStorage.getItem("token")) fetchLocalStorage();

    function logout(next){
      return $http.post(config.API.baseUrl+'logout')
      .success(function(response){
        localStorage.clear();
        service.user = {};
        service.isAuthenticated = false;
        service.logged = false;
        $http.defaults.headers.common['Authorization']='';
        if (!!next) next();
      })
      .error(function(response){
        if (!!next) next();
      })
    }

    function register(fields,file,next){
      $upload.upload({
          url: config.API.baseUrl+'register', // upload.php script, node.js route, or servlet url
          method: 'POST',
          headers: {'Authorization': 'bearer '+localStorage.getItem("token")}, // only for html5
          file: file, // single file or a list of files. list is only for html5,
          data: fields,
        }).progress(function(evt) {
          $rootScope.progressBar = parseInt(100.0 * evt.loaded / evt.total);
        }).success(function(data, status, headers, config) {
          if (!!next){
            if (status==200) next(null,data);
            else next(data);
          }
        });
      }

      function login(user,pass,next) {
        service.logged = false;
        service.isAuthenticated = false;
        clearLocalStorage();
        $http.defaults.headers.common['Authorization']='';

        return $http.post(config.API.baseUrl+'login?username='+user+'&password='+pass)
        .success(function(response){

          if (!!response.token){

            localStorage.setItem("token",response.token);
            localStorage.setItem("uid",response.id);
            localStorage.setItem("user_role",response.role);
            localStorage.setItem("username",response.username);
            localStorage.setItem("email",response.email);
            localStorage.setItem("clientCode",response.clientCode);
            fetchLocalStorage();
            $http.defaults.headers.common['Authorization']='bearer '+localStorage.getItem("token");
            
          }
          if (!!next) next();
        })
        .error(function(response){
          if (!!next) next();
        })
      }
      
      function clearLocalStorage(){
        localStorage.removeItem('token');
        localStorage.removeItem('uid');
        localStorage.removeItem('user_role');
        localStorage.removeItem('username');
        localStorage.removeItem('email');
        localStorage.removeItem('clientCode');
      }

      function fetchLocalStorage(){
        //fill the factory from localstorage.
        service.user = {
          username: localStorage.getItem('username'),
          role: localStorage.getItem('user_role'),
          uid: localStorage.getItem('uid'),
          email: localStorage.getItem('email'),
          clientCode: localStorage.getItem('clientCode'),
        }
        service.token = localStorage.getItem('token');
        service.isAuthenticated = true;
        $http.defaults.headers.common['Authorization']='bearer '+localStorage.getItem("token");
      }

      function isAuthorized(roles){
        //check if the user is authorized for the roles provided
        if (!service.user) return false;
        else if (roles.indexOf(service.user.role)!=-1) return true;
        else return false;
      }
      function getRole(){
        if (!service.isAuthenticated) return "client";
        else if ((!service.user) || (!service.user.role)) return "client";
        else return service.user.role;
      }

      return service;
      
    }



})();