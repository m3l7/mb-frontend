(function(){
    angular
        .module('app')
        .run(langDetect);

        langDetect.$inject = ['config', 'langUtils'];

        function langDetect(config, langUtils){
            if ((!!navigator.language) && (config.lang.langDetect)){
                var lang = navigator.language.slice(0,2).toLowerCase();
                if (config.lang.langs.indexOf(navigator.language)!=-1) {
                    langUtils.changeLang(lang);
                }
                else langUtils.changeLang(config.lang.defaultLang);
            }
        }
})();