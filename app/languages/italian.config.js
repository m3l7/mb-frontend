    (function(){
        angular
        .module('app')
        .config(italianLanguage);

        function italianLanguage($translateProvider){

            //set the path for languages
            $translateProvider.useStaticFilesLoader({
              prefix: '/languages/',
              suffix: '.json'
          });

            $translateProvider.translations('it', {

                //INFO
                _SEDE_LEGALE: "Sede legale",
                _SEDE_OPERATIVA: "Sede operativa",

                //VISION & MISSION
                _MISSION_TEXT: "NOI CREDIAMO CHE OGNI RADIATORE ABBIA IL DIRITTO DI AVERE UN SISTEMA DI FISSAGGIO, <strong>ELEGANTEMENTE FUNZIONALE E CONCRETAMENTE SICURO</strong>.",
                _VISION_TEXT: "CREIAMO SOLUZIONI DI FISSAGGIO PER OGNI ESIGENZA DI MONTAGGIO, <strong> NELL’IMPRESCINDIBILE VINCOLO FRA SICUREZZA</strong> ED <strong>ELEGANZA ESTETICA</strong>,GRAZIE ALLE COMPETENZE CHE SOLO NOI <strong>ITALIANI</strong> SIAMO CAPACI DI OFFRIRE, NE È GARANZIA LA NOSTRA STORIA TESTIMONIATA DAL RICONOSCIMENTO DEI NOSTRI CLIENTI E <strong> DALLA LORO FIDUCIA</strong>.",

                // VDI
                _VDI_FIRST_SECTION: "<p>MB HA SVILUPPATO DIVERSE SOLUZIONI DI FISSAGGIO, SIA PER PARETE, SIA PER IL PAVIMENTO, CONFORMI ALLA CLASSE 3 DELLA DIRETTIVA TEDESCA <strong>VDI 6036</strong> (IL PIÙ ALTO LIVELLO DI SICUREZZA, NON COMPRENDE PERÒ INSTALLAZIONI SU LUOGHI PARTICOLARI COME LE PRIGIONI E GLI ISTITUTI PSICHIATRICI). PER CIASCUNA STAFFA, UN’ESAURIENTE TABELLA (CHE TROVATE DISPONIBILE QUI PER IL DOWNLOAD) SPECIFICA QUANTI FISSAGGI DEVONO ESSERE USATI PER OGNI GRANDEZZA DI RADIATORE.</p>",
                _VDI_SECOND_SECTION: "<p>DEVE ESSERE SEMPRE PRESO IN CONSIDERAZIONE ANCHE IL MATERIALE CON CUI È STATO COSTRUITO IL MURO DOVE INSTALLARE IL RADIATORE; INFATTI, SE LA RESISTENZA DEL MURO NON DOVESSE ESSERE ADEGUATA, DEVONO ESSERE UTILIZZATE DELLE VITI E DEI TASSELLI SPECIALI E/O AUMENTARE IL NUMERO DI STAFFE.</p><p>UN AMPIO NUMERO DI STAFFE MB, PER VARI TIPI DI RADIATORI, VIENE TESTATO DAL TÜV SÜD PER ATTESTARE LA CONFORMITÀ ALLA DIRETTIVA VDI 6036, TUTTO QUESTO PER OFFRIRE AI NOSTRI CLIENTI UNA MAGGIORE SICUREZZA E FIDUCIA NEI NOSTRI PRODOTTI.</p><p>ANCHE IL NOSTRO TEAM HA TESTATO LA RESISTENZA DEI NOSTRI FISSAGGI IN CONDIZIONI DAVVERO PARTICOLARI – DATE UN’OCCHIATA AL VIDEO!</p>",

                // VDI MATRIX
                _VDI_MATRIX_HEADER: "<p>Trova il tuo sistema di fissaggio <strong>VDI6036</strong>!</p>",

                //RING
                _RING_INNER: "<li><span><strong>PERCHÉ</strong></span></li><li><span>Affrontiamo i problemi</span></li><li><span>con creatività</span></li><li><span class='blue'>MB Fix it!</span></li>",
                _RING_MIDDLE: "<li><span><strong>COME</strong></span></li><li><span>Prodotti personalizzati, concretamente sicuri</span></li><li><span>e elegantemente funzionali</span></li>",
                _RING_OUTER: "<li><span><strong>COSA</strong></span></li><li><span>Progettiamo sistemi di fissaggio</span></li>",


                //CONTATTI
                _WELCOME: 'Benvenuti in MB Srl',
                _HOME_TEXT: '"Da oltre quarant\'anni noi vogliamo che ogni radiatore abbia il diritto ad un fissaggio elegantemente funzionale e concretamente sicuro."',
                _HOME_VIDEOS: 'Video',
                _HOME_CONTACTS: 'Contatti',


                //HOME
                _GIORNI_RECLAMO: "giorni dall'ultimo reclamo",
                _HOME_QUOTE: "Più di <strong>{{radiatorsNumber}}</strong> radiatori sono stati installati in sucurezza con i sistemi di fissaggio <strong>MB</strong>",

                //HEADER
                _PRODOTTI: 'Prodotti',
                _INFO_VDI: "Info VDI",
                _MOTORI: "Motori di scelta",
                _CONTATTI: "Contatti",
                _VISION: "Vision",
                _SERVIZI: "Servizi",
                _CERTIFICATI: "Certificati",
                _AREA_CLIENTI: "Area Privata",
                _PRIVACY: "Privacy",
                _CATALOGO: "Catalogo",
                _VIDEO: "Video",

                //REGISTRAZIONE
                _ISTRUZIONI_REGISTRAZIONE: "Compila il form sottostante e carica il <a href='/content/doc/FORM NEW CLIENT.doc'>documento di registrazione</a>",
                _NOME_AZIENDA: "Nome azienda",
                _INDIRIZZO_FATTURAZIONE: "Indirizzo di fatturazione",
                _PARTITA_IVA: "Partita IVA",
                _EMAIL: "E-mail",
                _TELEFONO: "Telefono",
                _CARICA_FORM: "Carica Form",
                _REGISTRATI: "Registrati",



                //CATALOGHI
                _BATHROOM_RADIATORS: 'Radiatori da bagno',
                _ALUMINIUM_RADIATORS: 'Radiatori in alluminio',
                _CASTIRON_RADIATORS: 'Radiatori lamellari e in ghisa',
                _COLUMN_RADIATORS: 'Radiatori a colonna',
                _OTHER_FIXING: 'Altri sistemi di fissaggio',
                _STEEL_PANEL_RADIATORS: 'Radiatori con pannello in acciaio',

                //SERVIZI
                _SERVIZI_3D: "<h2>PIANIFICAZIONE 3D</h2><p>Sviluppiamo autonomamente progetti complessi a partire da un'idea del Cliente  sino ad arrivare alla realizzazione del prodotto finale.</p><p>Le nostre competenze vanno dalla capacità progettuale, supportata da tecnici specializzati e da strumenti all'avanguardia, sino alle prime prototipazioni.</p><p>Tutto questo grazie alla modellazione 3D utilizzando i principali software presenti sul mercato per realizzare modelli tridimensionali di qualsiasi tipologia e/o esigenza.</p>",
                _SERVIZI_OFFICINA_STAMPI: "<h2>OFFICINA STAMPI</h2><p>Offriamo un servizio completo per la progettazione degli stampi e delle attrezzature ausiliarie necessarie alle singole lavorazioni.</p><p>Ogni fase della progettazione viene seguita con la massima cura al fine di garantire un risultato efficiente in linea con le aspettative del cliente.</p><p>Una volta individuato il progetto più vantaggioso, si passa alla fase di progettazione meccanica vera e propria utilizzando software 3D. </p><p>Il processo di engineering permette, alla fine, di elaborare i disegni costruttivi, gli schemi di lavorazione e la realizzazione degli stampi.</p>",
                _SERVIZI_PLASTICA:"<h2>STAMPAGGIO PLASTICA</h2><p>Offriamo lavorazioni su materie plastiche tramite lo stampaggio ad iniezione confermandoci come partner ideale per la realizzazione di componenti tecnici in materiale plastico.</p><p>Il progetto del cliente trova quindi esecuzione nella fase di realizzazione del componente in plastica. Lo stampaggio industriale viene effettuato utilizzando i più moderni materiali in modo tale da garantire durabilità ed affidabilità degli articoli prodotti.</p>",
                _SERVIZI_LAMIERA: "<h2>STAMPAGGIO LAMIERA</h2><p>Grazie al reparto stampaggio lamiera presente in azienda possiamo garantire la produzione di lotti su misura per qualsiasi esigenza del cliente.</p><p>Lo stampaggio lamiera a freddo è realizzato tramite presse meccaniche ed idrauliche eccentriche fino a 250 tonnellate di potenza per la produzione di lamierati tranciati e imbutiti di vari spessori e dimensioni. </p><p>La varietà di potenze delle nostre presse ci permette una grande flessibilità di lavoro,  consentendoci di realizzare pezzi delle più svariate dimensioni.</p>",
                _SERVIZI_CONFEZIONAMENTO: "<h2>CONFEZIONAMENTO</h2><p>Il confezionamento dei prodotti finiti è un’operazione importante quanto la produzione stessa. Un buon confezionamento può far scegliere al cliente se acquistare i vostri prodotti o meno.</p><p>Per confezionamento intendiamo tutte le operazioni di allestimento di un prodotto nudo o preconfezionato fino alla sua completa realizzazione e pronto per la distribuzione al cliente e/o al punto vendita.</p><p>Offriamo servizio di confezionamento manuale, semiautomatico o completamente automatico.</p>",
                _SERVIZI_STAFFE: "<h2>LINEA STAFFE</h2><p>Offriamo la possibilità di completare la produzione di alcuni componenti grazie all'utilizzo di automazioni sull'assemblaggio e il confezionamento. </p><p>Questa tipologia di attività ci consente di completare la produzione di specifici articoli, destinati al settore del riscaldamento, dalla materia prima fino al prodotto finito riducendo le tempistiche ed aumentando la qualità offerta alla nostra clientela.</p>",
                _SERVIZI_RIPRESE: "<h2>RIPRESE</h2><p>Per completare ogni tipologia di lavorazione e per venire incontro a tutte le esigenze del cliente, l'azienda offre la possibilità di lavorazioni particolari tramite il reparto delle riprese.</p><p>Sono presenti in questo reparto alcune presse meccaniche di potenza da 10 a 20 tonnellate attraverso le quali sarà possibile ottimizzare ogni lavorazione su richiesta.</p>",


                _REGISTRAZIONE_IN_ELABORAZIONE: "Richiesta in elaborazione. Attendere.",
                _REGISTRAZIONE_IN_CONVALIDA: 'Registrazione in attesa di convalida. L\'esito verrà inviato via e-mail',
                _USERNAME: 'Username',
                _NON_REGISTRATO: 'Non sei ancora registrato?',
                _EMAIL_NON_VALIDA: 'Email non valida',


            });


    $translateProvider.translations('en', {

                //INFO
                _SEDE_LEGALE: "Registered headquarters",
                _SEDE_OPERATIVA: "Operative headquarters",

                //VISION & MISSION
                _MISSION_TEXT: "WE BELIEVE EVERY RADIATOR HAS THE RIGHT TO HAVE AN <strong>ELEGANT</strong>, <strong>FUNCTIONAL</strong> AND ABSOLUTELY <strong>SAFE</strong> FASTENING SYSTEM.",
                _VISION_TEXT: "WE CREATE FIXING SOLUTIONS FOR ALL MOUNTING SITUATIONS, <strong>KEEPING TOGETHER SAFETY</strong> AND <strong>AESTHETICS</strong>, THANKS TO THE COMPETENCES THAT WE <strong>ITALIANS</strong> ARE ABLE TO OFFER, TESTIFIED BY OUR HISTORY AND THE CONSTANT ACKNOWLEDGEMENT AND <strong>TRUST OF OUR COSTUMERS</strong>.",

                // VDI
                _VDI_FIRST_SECTION: "<p>The recent <strong>VDI6036</strong> directive is changing the way installers, engineers and architects think of <strong>safety in mounting radiators</strong>. Depending on the application and placement of the radiator, limits are set to forces and displacements, taking into account also anomalous uses, to avoid any risk of damage to people and things.</p><p>In public places like hospitals, schools, banks, stadiums, etc. a higher standard is required. </p>",
                _VDI_SECOND_SECTION: "<p>MB developed several dedicated solutions, both for wall and floor mounting, complying to class 3 of the directive (the highest safety level, apart from individually defined prescriptions for very peculiar installation situations such as prisons and psychiatric institutes). For each bracket, a comprehensive table specifying how many brackets have to be used for each radiator size is available for <a href='/content/doc/Numero mensole VDI compatto - febbraio 2015 - inglese.xls'>download here</a>.</p><p>The wall material must always be taken into account. If the wall resistance is not adequate, special screws and plugs must be used and/or the number of brackets must be increased.</p><p>We let TÜV SÜD verify the conformity to the directive of a comprehensive list of brackets for all radiator types and applications, for extra safety and confidence for our customers.</p><p>And we let also our special team test the resistance of the brackets to very unusual conditions – just have a look at the video!</p>",

                // VDI MATRIX
                _VDI_MATRIX_HEADER: "<p>Find your perfect <strong>VDI6036</strong> fixing system!</p>",

                //RING
                _RING_INNER: "<li><span><strong>WHY</strong></span></li><li><span>Face problems</span></li><li><span>with creativity</span></li><li><span class='blue'>MB Fix it!</span></li>",
                _RING_MIDDLE: "<li><span><strong>HOW</strong></span></li><li><span>Dedicated items, both functional</span></li><li><span>and aesthetically pleasing</span></li>",
                _RING_OUTER: "<li><span><strong>WHAT</strong></span></li><li><span>We design fastening systems</span></li>",


                //CONTATTI
                _WELCOME: 'Benvenuti in MB Srl',
                _HOME_TEXT: '"Da oltre quarant\'anni noi vogliamo che ogni radiatore abbia il diritto ad un fissaggio elegantemente funzionale e concretamente sicuro."',
                _HOME_VIDEOS: 'Video',
                _HOME_CONTACTS: 'Contacts',

                //HOME
                _GIORNI_RECLAMO: "days since last complaint",
                    // _HOME_QUOTE: "More than <strong>34,600,000</strong> radiators have been safely installed with <strong>MB</strong> brackets",
                    _HOME_QUOTE: "More than <strong>{{radiatorsNumber}}</strong> radiators have been safely installed with <strong>MB</strong> brackets",


                //HEADER
                _PRODOTTI: 'Products',
                _INFO_VDI: "VDI Info",
                _MOTORI: "Choose engines",
                _CONTATTI: "Contacts",
                _VISION: "Vision & Mission",
                _SERVIZI: "Services",
                _CERTIFICATI: "Certificates",
                _AREA_CLIENTI: "Private Area",
                _PRIVACY: "Privacy",
                _CATALOGO: "Catalogue",
                _VIDEO: "Video",
                _AREA_RISERVATA: 'Reserved Area',

                //REGISTRAZIONE
                _ISTRUZIONI_REGISTRAZIONE: "Fill out the form below and upload the <a href='/content/doc/FORM NEW CLIENT.doc'>registration document</a>",
                _NOME_AZIENDA: "Company name",
                _INDIRIZZO_FATTURAZIONE: "Invoicing Address",
                _PARTITA_IVA: "VAT Code",
                _EMAIL: "E-mail",
                _TELEFONO: "Phone",
                _CARICA_FORM: "Upload Form",
                _REGISTRATI: "Register",

                //CATALOGHI
                _BATHROOM_RADIATORS: 'Bathroom Radiators',
                _ALUMINIUM_RADIATORS: 'Aluminium radiators',
                _CASTIRON_RADIATORS: 'Cast-iron and lamellar radiators',
                _COLUMN_RADIATORS: 'Column radiators',
                _OTHER_FIXING: 'Other fixing systems',
                _STEEL_PANEL_RADIATORS: 'Steel panel radiators',

                //SERVIZI
                _SERVIZI_3D: "<h2>3D PLANNING</h2><p>Autonomously, we develop complex projects starting from the customer ideas up to the realisation of the finished product.</p><p>Our competences range from planning ability, well supported by qualified technicians  and by the state-of-the-art of our equipments, to the first prototyping.</p><p>All this, thanks to 3D modeling and using the main softwares currently on the market, in order to realise every typology of three-dimensional model, based on customer’s requirements.</p>",
                _SERVIZI_OFFICINA_STAMPI: "<h2>MOULDS WORKSHOP</h2><p>We offer a complete service for moulds and auxiliary equipments designing necessary for each process.</p><p>Every design phase is overseen with the maximum attention to guarantee an efficient result that meets customers expectation.</p><p>Once we have identified the most advantageous project, we proceed with the mechanical design phase utilising the 3D software.</p><p>Finally/At the end, the engineering process allows/permits to elaborate the construction drawings, the working plans/schemas and the moulds realisation.</p>",
                _SERVIZI_PLASTICA:"<h2>PLASTIC MOULD</h2><p>We offer production of plastic materials by injection moulding, making MB as an ideal partner for the realisation of technical components in plastic material. </p><p>The customer project finds execution in the realisation phase of the plastic element. The industrial moulding is made utilising the most modern materials thus guaranteeing durability and reliability of the manufactured products.</p>",
                _SERVIZI_LAMIERA: "<h2>SHEET METAL PRESSING</h2><p>Thanks to the sheet metal pressing department present in the company/factory, we can guarantee the production of custom-made lots meeting every customers needs.</p><p>The cold metal sheet pressing is realised through mechanical and eccentric hydraulic press that can reach up to 250 tons of power for the production of sheet metal blanking, drawing or cupping of various/different thicknesses and sizes.</p><p>The variety of our presses powers grants us great working flexibility, allowing us to create pieces of the most various sizes.</p>",
                _SERVIZI_CONFEZIONAMENTO: "<h2>PACKAGING</h2><p>The packaging of the finished product is as important as the production itself.  A good packaging can make the client choose whether to buy your products or not.</p><p>For packaging we mean all the operations of preparation of a loose or a pre-packaged product until its complete realisation and it’s ready for the distribution to the customers and/or sales point. </p><p>We offer manual, semi-automatic or fully automatic packaging service.</p>",
                _SERVIZI_STAFFE: "<h2>BRACKETS LINE</h2><p>We offer the possibility to complete some components production using assembly and packaging automation.</p><p>This type of activity allows us to finish the manufacturing of specific heating branch articles, from the raw materials to the finished product by reducing the required time and increasing the quality offered to our customers.</p>",
                _SERVIZI_RIPRESE: "<h2>TOOLS AND MACHINERY</h2><p>To complete each type of processing and to meet all customer needs, the company offers some special manufacturing through the machinery department.</p><p>In this department can be found some mechanical presses which power can range from 10 to 20 tons that allow to optimize every customised client request.</p>",


                _REGISTRAZIONE_IN_ELABORAZIONE: "Request waiting to be processed",
                _REGISTRAZIONE_IN_CONVALIDA: 'Request waiting to be validated. The result will be sent by mail',
                _USERNAME: 'Username',
                _NON_REGISTRATO: 'Not registered yet?',
                _EMAIL_NON_VALIDA: 'Email not valid',


            });

    $translateProvider.translations('fr', {

                // RING
                _RING_INNER: "<li><span><strong>POURQUOI</strong></span></li><li><span>Nous abordons les problèmes</span></li><li><span>avec créativité</span></li><li><span class='blue'>MB Fix it!</span></li>",
                _RING_MIDDLE: "<li><span><strong>COMMENT</strong></span></li><li><span>Produits personnalisés, effectivement sûrs,</span></li><li><span>élégants et fonctionnels </span></li>",
                _RING_OUTER: "<li><span><strong>QUOI</strong></span></li><li><span>Nous réalisons des systèmes de fixation</span></li>",

                //CONTATTI
                _WELCOME: 'Bienvenus chez MB',
                _HOME_TEXT: 'Depuis 40 ans nous croyons que chaque radiateur a le droit d\'avoir une fixation elegante, fonctionnelle et abolument sure.',
                _HOME_VIDEOS: 'Vidéo',
                _HOME_CONTACTS: 'Contactez-nous',

                //HOME
                _GIORNI_RECLAMO: "Dernière réclamation",
                _HOME_QUOTE: "Plus de <strong>{{radiatorsNumber}}</strong> radiateurs ont ete installe en toute surete avec consoles <strong>MB</strong>",


                //HEADER
                _PRODOTTI: 'Produits',
                _INFO_VDI: "Info VDI",
                _MOTORI: "Outils de sélection",
                _CONTATTI: "Contactez-nous",
                _VISION: "Vision",
                _SERVIZI: "Services",
                _CERTIFICATI: "Certifications",
                _AREA_CLIENTI: "Espace clients",
                _PRIVACY: "Protection des données",
                _CATALOGO: "Catalogue",
                _VIDEO: "Vidéo",
                _AREA_RISERVATA: 'Espace réservé',

                // VDI MATRIX
                _VDI_MATRIX_HEADER: "<p>Trouvez votre parfait <strong>VDI6036</strong> système de fixation!</p>",

                // VDI
                _VDI_FIRST_SECTION: "<p>LA RECENTE DIRECTIVE ALLEMANDE <strong>VDI 6036</strong> EST EN TRAIN DE CHANGER LA PENSEE DES INSTALLATEURS ET DE PRESCRIPTEURS SUR <strong>LA SECURITE DU MONTAGE</strong> DES RADIATEURS. ON A POSE DES LIMITES PRECISES SUR LES FORCES ET LES DEPLACEMENTS ADMISSIBLES POUR EVITER TOUT RISQUE DE DOMMAGE AUX PERSONNES OU AUX CHOSES. DANS LES LIEUX OUVERTS AU PUBLIC, ON DEMANDE UN STANDARD PLUS ELEVE.</p>",
                _VDI_SECOND_SECTION: "<p>MB A DÉVELOPPÉ PLUSIEURS SOLUTIONS DE FIXATION, SOIT POUR LE MONTAGE AU MUR, SOIT POUR LE MONTAGE AU SOL; CES SOLUTIONS DE FIXATION SONT CONFORMES À LA CLASSE 3 DES RECOMMANDATIONS ALLEMANDES <strong>VDI 6036</strong> (LE PLUS HAUT NIVEAU DE <strong>SÉCURITÉ</strong>, EXCEPTION FAITE POUR L'INSTALLATION DANS DES ENDROITS SPÉCIAUX COMME LES PRISONS ET LES ÉTABLISSEMENTS PSYCHIATRIQUES). POUR CHAQUE MODÈLE DE CONSOLE, UN TABLEAU COMPLET (QUE VOUS POUVEZ TÉLÉCHARGER ICI) SPÉCIFIE COMBIEN DES FIXATIONS DOIVENT ÊTRE UTILISÉES SELON LA TAILLE DU RADIATEUR.</p><p>ON NE DOIT JAMAIS OUBLIER DE CONSIDÉRER LE MATÉRIAU AVEC LEQUEL LE MUR OÙ VOUS INSTALLEREZ LE RADIATEUR A ÉTÉ CONSTRUIT; EN FAIT, AU CAS OÙ LA RÉSISTANCE DU MUR N’EST PAS SUFFISANTE, ON DEVRA UTILISER DES VIS ET CHEVILLES SPÉCIALES ET / OU AUGMENTER LE NOMBRE DES CONSOLES.</p><p>PLUSIEURS CONSOLES MB, POUR UNE VASTE GAMME DE TYPES DE RADIATEURS, ONT ÉTÉ VÉRIFIÉES PAR TÜV SÜD POUR ATTESTER LEUR CONFORMITÉ AUX RECOMMANDATIONS ALLEMANDES VDI 6036, AFIN DE DONNER CONFIANCE ET SÉCURITÉ EXTRA À NOS CLIENTS. AUSSI NOTRE ÉQUIPE A TESTÉ LES CONSOLES DANS DES SITUATIONS TRÈS ÉLOIGNÉES DE LEUR UTILISATION NORMALE - REGARDEZ NOTRE VIDÉO!</p>",

                //REGISTRAZIONE
                _ISTRUZIONI_REGISTRAZIONE: "Remplissez le formulaire et téléchargez le <a href='/content/doc/FORM NEW CLIENT.doc'>document d'enregistrement</a>",
                _NOME_AZIENDA: "Nom de l'entreprise",
                _INDIRIZZO_FATTURAZIONE: "Adresse de facturation",
                _PARTITA_IVA: "Numéro de TVA",
                _EMAIL: "Adresse électronique",
                _TELEFONO: "Tél.",
                _CARICA_FORM: "Télécharger",
                _REGISTRATI: "Enregistrer",

                //CATALOGHI
                _BATHROOM_RADIATORS: 'Sèche-serviettes',
                _ALUMINIUM_RADIATORS: 'Radiateurs en fonte d\'aluminium',
                _CASTIRON_RADIATORS: 'Radiateurs en fonte et lamellaires',
                _COLUMN_RADIATORS: 'Radiateurs colonne',
                _OTHER_FIXING: 'Autres systèmes de fixation',
                _STEEL_PANEL_RADIATORS: 'Panneaux en acier',

                // VISION & MISSION
                _VISION_TEXT: "NOUS CREONS DES SOLUTIONS DE FIXATION POUR TOUS LES SITUATIONS DE MONTAGE, <strong>MAINTENANT ENSEMBLE LA SÉCURITÉ</strong> ET <strong>L’ESTHÉTIQUE</strong>, GRÂCE AUX COMPÉTENCES QUE NOUS <strong>ITALIENS</strong> SOMMES CAPABLES D'OFFRIR, MIS EN ÉVIDENCE AUSSI PAR L'HISTOIRE, LA RECONNAISSANCE DE NOS CLIENTS ET <strong>DE LEUR CONFIANCE</strong>.",
                _MISSION_TEXT: "NOUS CROYONS QUE CHAQUE RADIATEUR A LE DROIT D'AVOIR UN<strong> SYSTÈME DE FIXATION</strong>, <strong>ÉLÉGANT</strong>, FONCTIONNEL ET PARFAITEMENT <strong>SÛRE</strong>.",

                //SERVIZI
                _SERVIZI_3D: "<h2>MODELISATION 3D</h2><p>Nous développons de manière autonome des projets complexes à partir d'une idée du client jusqu'à arriver au produit final. Nos compétences vont de la capacité de conception, soutenue par des techniciens spécialisés et des outils de pointe, jusqu'aux premiers prototypes. Tout cela grâce à la modélisation 3D en utilisant un des principaux logiciels sur le marché pour produire des modèles en trois dimensions de tout type et / ou besoin.</p>",
                _SERVIZI_OFFICINA_STAMPI: "<h2>ATELIER MOULES</h2><p>Nous offrons un service complet pour la conception des moules et des équipements auxiliaires nécessaires. Chaque étape du projet est suivie avec grand soin afin d'assurer un résultat efficace en ligne avec les attentes des clients. Dès que le projet le plus convenable aux attentes du client a été choisi, on passe à l’étape de la réalisation en utilisant un logiciel 3D. Le processus d’engineering permet, finalement, d’élaborer les dessins de construction, les plans de travail et la réalisation de moules.</p>",
                _SERVIZI_PLASTICA:"<h2>MOULAGE PLASTIQUE</h2><p>Nous mettons à disposition de nos clients aussi l'usinage de matières plastiques par moulage par injection, ce qui nous confirme comme partenaire idéal pour la réalisation de pièces techniques en matière plastique. Le projet du client trouve exécution dans la phase de mise en œuvre du composant plastique. Le moulage industriel est réalisé en utilisant les matériaux les plus modernes, ainsi d’assurer la durabilité et la fiabilité des articles fabriqués.</p>",
                _SERVIZI_LAMIERA: "<h2>MOULAGE TOLE</h2><p>Grâce au département tôle au cœur de notre l'entreprise nous pouvons garantir la production de lots adaptés à toutes les exigences de la clientèle. Le moulage à froid de la tôle est réalisé avec presses mécaniques et hydrauliques excentriques jusqu'à 250 tonnes de puissance pour la fabrication de tôles tranchées et d’emboutis en diverses tailles et épaisseurs. La variété des puissances de nos presses nous offre une grande flexibilité de travail, ce que nous permet de créer des pièces de dimensions les plus variées.</p>",
                _SERVIZI_CONFEZIONAMENTO: "<h2>CONDITIONNEMENT</h2><p>L'emballage des produits finis est une opération aussi importante que la fabrication elle-même. Un bon conditionnement peut faire décider à votre client si acheter vos produits ou non. Pour conditionnement nous entendons toutes les opérations de préparation d'un produit nu ou préemballé jusqu'à sa réalisation complète et prête pour la distribution dans le point de vente. Nous proposons un conditionnement manuel, semi-automatique ou entièrement automatique.</p>",
                _SERVIZI_STAFFE: "<h2>GAMME CONSOLES</h2><p>Nous offrons la possibilité de compléter la fabrication de certains composants grâce à l'utilisation de l'automatisation dans l'assemblage et l'emballage. Ce type d'activité nous permet de compléter la fabrication d'articles spécifiques, destinés au secteur du chauffage, en partant de la matière première pour arriver au produit fini en réduisant le temps de fabrication et en augmentant la qualité offerte à nos clients.</p>",
                _SERVIZI_RIPRESE: "<h2>REPRISE</h2><p>Pour compléter chaque type d’usinage et pour répondre aux besoins des clients les plus divers, l’entreprise offre la possibilité d’une élaboration spéciale à travers la division de reprise. Dans ce département sont présents des presses avec puissance mécanique de 10 à 20 tonnes à travers lesquelles il sera possible d'optimiser chaque usinage sur demande.</p>",


                _REGISTRAZIONE_IN_ELABORAZIONE: "Requête en attente de traitement",
                _REGISTRAZIONE_IN_CONVALIDA: 'Requête enregistrée. Vous recevrez une réponse par courrier électronique.',
                _USERNAME: 'Identifiant',
                _NON_REGISTRATO: 'Pas encore enregistré',
                _EMAIL_NON_VALIDA: 'Adresse non valide',

                _SEDE_LEGALE: "Siège social",
                _SEDE_OPERATIVA: "Site de production",
            })


    $translateProvider.translations('de', {

        //INFO
        _SEDE_LEGALE: "Sitz der Gesellschaft",
        _SEDE_OPERATIVA: "Betriebsniederlassung",

        // VDI
        _VDI_FIRST_SECTION: "<p>DIE NEUE RICHTLINIE <strong>VDI 6036</strong> LÄSST PLANER, ARCHITEKTEN UND HEIZUNGSBAUER IHRE SICHT AUF SICHERHEIT FÜR DIE HEIZKÖRPERMONTAGE ÄNDERN. ABHÄNGIG VON DER PLATZIERUNG UND DER VERWENDUNG DES HEIZKÖRPERS WERDEN BESTIMMTE GRENZEN FÜR DIE KRÄFTE UND ZUGELASSENE BEWEGUNG DEFINIERT. IN ÖFFENTLICHEN GEBÄUDEN WIE KRANKENHÄUSER, SCHULEN USW. WIRD EIN ERHÖHTER STANDARD, ANWENDUNGSKLASSE 3, GEFORDERT.</p>",
        _VDI_SECOND_SECTION: "<p>MB HAT UNTERSCHIEDLICHE LÖSUNGEN FÜR WAND- UND BODENMONTAGE ENTWICKELT, DIE KONFORM GEHEN MIT DER VDI 6036, ANWENDUNGSKLASSE 3, D.H. AUCH DEN HOHEN SICHERHEITSANFORDERUNGEN WIE Z. B. IN SCHULEN UND KINDERGÄRTEN GERECHT WERDEN.</p><p>FÜR JEDEN KONSOLENTYP <a href='/content/doc/Numero mensole VDI compatto - febbraio 2015 - inglese.xls'>STEHT HIER</a> KOMPLETTE TABELLE, DIE DIE EMPFOHLENE KONSOLENANZAHL FÜR JEDE HEIZKÖRPERGRÖSSE ANGIBT, ZUR VERFÜGUNG.</p><p>IN JEDEM FALL SIND ZUR HEIZKÖRPERBEFESTIGUNG, DER ENTSPRECHENDE KONSOLENTYP UND DEREN ANZAHL SOWIE DIE ZAHL DER SCHRAUBEN UND DÜBEL AUFGRUND DER BESCHAFFENHEIT DES BEFESTIGUNGSUNTERGRUNDES UND DER ZUSÄTZLICHEN, EXTERNEN BELASTUNG AUSZUWÄHLEN.</p><p>DER TÜV SÜD PRÜFT FÜR DIE VERSCHIEDENEN MB-BEFESTIGUNGSMODELLE DIE KONFORMITÄT NACH DERANWENDUNGSKLASSE 3 DER RICHTLINIE, UM FÜR JEDEN HEIZKÖRPER UND JEDE ANWENDUNG UNSEREN KUNDEN EIN EXTRA AN SICHERHEIT UND VERTRAUEN GEBEN ZU KÖNNEN.</p><p>DER TÜV SÜD PRÜFT FÜR DIE VERSCHIEDENEN MB-BEFESTIGUNGSMODELLE DIE KONFORMITÄT NACH DERANWENDUNGSKLASSE 3 DER RICHTLINIE, UM FÜR JEDEN HEIZKÖRPER UND JEDE ANWENDUNG UNSEREN KUNDEN EIN EXTRA AN SICHERHEIT UND VERTRAUEN GEBEN ZU KÖNNEN.</p>",

        //VISION & MISSION
        _MISSION_TEXT: "UNSERE AUFGABE BESTEHT DARIN, FÜR JEDE HEIZKÖRPERART EINE PASSENDE, TECHNISCH AUSGEREIFTE, OPTISCH ANSPRECHENDE UND ABSOLUT SICHERE BEFESTIGUNG ZU BIETEN",
        _VISION_TEXT: "EIN KOMPETENTER PARTNER FÜR BEFESTIGUNGSSYSTEME IM HEIZKÖRPERBEREICH UND ANDEREN ÄNWENDUNGSGEBIETEN, MIT DEM FOKUS AUF:<li>SICHERHEIT UND ÄSTHETIK</li><li>ITALIENISCHE KOMPETENZ</li><li>IN UNSERER HISTORIE FÜR DIE ZUKUNFT VERPFLICHTEND</li><li>DAS VERTRAUEN UND DIE ANERKENNUNG UNSERER GESCHÄFTSPARTNER IN DER</li><li>VERGANGENHEIT, WIE BEWIESEN, UND IN DER ZUKUNFT GEMEINSAM DEN ERFOLGZU TEILEN</li>",

        //SERVIZI
        _SERVIZI_3D: "<h2>ENTWICKLUNG</h2><p>Unser R&D Abteilung entwickelt komplexe Projekte, von der Kundenidee bis zur Fertigung des Produktes. Nach der 3D Planungsphase mit neuester Software, folgt die erste Bemusterung, für jede Art von 3D Modellen und Komponenten.</p>",
        _SERVIZI_OFFICINA_STAMPI: "<h2>FORMENENTWICKLUNG</h2><p>Wir bieten unseren Kunden eine komplette Dienstleistung für die Entwicklung und Produktion von Formen an.</p><p>Die ganze Designphase wird sorgfältig durchgeführt, um ein effektives Ergebnis für eine problemlose Produktion aller Komponenten zu garantieren.</p><p>Wenn das vorteilhafte Projekt gefunden ist, werden wir die 3D Planung, die konstruktionsfähigen Zeichnungen und die Produktion der Form fortführen.</p>",
        _SERVIZI_PLASTICA:"<h2>KUNSTSTOFFSPRITZGIESSEN</h2><p>Wir sind in der Lage, Komponenten aus Kunststoff durch Spritzgießen zu fertigen.</p><p>Mit unserer Erfahrung und Kompetenz in der Fertigung von Formen sind wir der ideale Partner für neue Projekte aber auch für schon existierende technische Komponenten aus Kunststoff. </p><p>Durch die Auswahl des optimalen Materials und der dadurch garantierten Haltbarkeit und Zuverlässigkeit, können wir jedes Kundenprojekt konkret umsetzen.</p>",
        _SERVIZI_LAMIERA: "<h2>BLECHPRESSEN</h2><p>Mit unseren Blechpressen sind wir in der Lage, Standard- und Sonderkomponenten für diverse Anwendungen nach Kundenwunsch zu fertigen.</p><p>Mit einer Kraft von bis zu 250 Tonnen werden durch Ausschneiden, Ziehen und Kelchanpressen Stücke mit unterschiedlichsten Formen und Maßen produziert.</p><p>Mit unseren vielfältigen Möglichkeiten der Blechverarbeitung garantieren wir Flexibilität in der Produktion und kurze Lieferzeiten.</p>",
        _SERVIZI_CONFEZIONAMENTO: "<h2>VERPACKUNG</h2><p>Die Verpackung eines Endproduktes ist oft so wichtig wie das Produktionsverfahren selbst. Eine vernünftige Verpackung kann der entscheidende Faktor für die Produktwahl sein.</p><p>Karton, Blisterpackung, Kunststoff sind nur einige der heute angewendeten Verpackungsarten bei MB. Wir können unseren Partner nicht nur eine große Auswahl von Materialien, sondern auch von Produktionsarten (manuell, halbautomatisch oder vollautomatisch) anbieten.</p>",
        _SERVIZI_STAFFE: "<h2>PRODUKTIONSERGÄNZUNG</h2><p>Die Vielfalt von Materialien, die MB als Standard bei der Konsolenherstellung benutzt, kann auch als Ergänzung von Kundenprodukten gebraucht werden.</p><p>Dadurch können die Investitionskosten und Einführungszeiten reduziert werden; das geht zusammen mit einer Garantie an Qualität. Montage und Verpackung sind dann automatisiert.</p>",
        _SERVIZI_RIPRESE: "<h2>SONDERFERTIGUNGEN</h2><p>Als Ergänzung zu jedem Produktionsprozess bietet MB die Möglichkeit, Sonder- oder Standardbearbeitungen für unsere Kunden zu erledigen. Unser Bearbeitungszentrum verfügt dafür über mechanische Pressen mit einer Kraft von 10 bis 20 Tonnen.</p>",

        //RING
        _RING_INNER: "<li><span><strong>WARUM</strong></span></li><li><span>PROBLEME MIT KREATIVITÄT LÖSEN</span></li><li><span class='blue'>MB Fix it!</span></li>",
        _RING_MIDDLE: "<li><span><strong>WIE</strong></span></li><li><span>DURCH SPEZIFISCHE ARTIKEL,</li><li><span> FUNKTIONELL UND</span></li><li><span>AUF DEM STAND DER TECHNIK</span></li>",
        _RING_OUTER: "<li><span><strong>WAS</strong></span></li><li><span>WIR LEBEN BEFESTIGUNGSSYSTEME</span></li>",

        //CONTATTI
        _WELCOME: 'Willkommen bei MB',
        _HOME_TEXT: '"Seit den 70er Jahren, wir glauben, dass jeder Heizkörper eine elegante, funktionelle und absolut sichere Befestigung haben soll."',
        _HOME_VIDEOS: 'Videos',
        _HOME_CONTACTS: 'Kontakt',

        //HOME
        _GIORNI_RECLAMO: "Tage ab der letzten Reklamation",
        _HOME_QUOTE: "Mehr als <strong>{{radiatorsNumber}}</strong> Heizkörper sind mit <strong>MB</strong> Befestigungen in aller Sicherheit installiert worden",


        //HEADER
        _PRODOTTI: 'Produkte',
        _INFO_VDI: "VDI 6036",
        _MOTORI: "Berechnungstool",
        _CONTATTI: "Kontakt",
        _VISION: "Vision",
        _SERVIZI: "Dienstleistungen",
        _CERTIFICATI: "Qualität",
        _AREA_CLIENTI: "Für Kunden",
        _PRIVACY: "Datenschutz",
        _CATALOGO: "Unterlagen",
        _VIDEO: "Videos",
        _AREA_RISERVATA: 'Reserviertes Bereich',

        //REGISTRAZIONE
        _ISTRUZIONI_REGISTRAZIONE: "Bitte füllen Sie das Formular und laden Sie den <a href='/content/doc/FORM NEW CLIENT.doc'>Anmeldungsdokument hinauf</a>",
        _NOME_AZIENDA: "Firma",
        _INDIRIZZO_FATTURAZIONE: "Adresse",
        _PARTITA_IVA: "Mehrwertsteuernummer",
        _EMAIL: "E-mail",
        _TELEFONO: "Telefon",
        _CARICA_FORM: "Formular senden",
        _REGISTRATI: "Anmelden",


        // VDI MATRIX
        _VDI_MATRIX_HEADER: "<p>Finden Sie Ihr perfektes <strong>VDI6036</strong> Befestigungssystem!</p>",

        //CATALOGHI
        _BATHROOM_RADIATORS: 'Badheizkörper',
        _ALUMINIUM_RADIATORS: 'Aluminium Heizkörper',
        _CASTIRON_RADIATORS: 'Guss- und Faltheizkörper',
        _COLUMN_RADIATORS: 'Röhrenheizkörper',
        _OTHER_FIXING: 'Andere Befestigungssyteme',
        _STEEL_PANEL_RADIATORS: 'Plattenheizkörper',

        _REGISTRAZIONE_IN_ELABORAZIONE: "Anfrage wird bearbeitet. Bitte warten.",
        _REGISTRAZIONE_IN_CONVALIDA: 'Afrage gespeichert. Sie werden eine Antwort per E-mail bekommen.',
        _USERNAME: 'Benutzername',
        _NON_REGISTRATO: 'Noch nicht angemeldet?',
        _EMAIL_NON_VALIDA: 'Ungültiges E-mail',

    })

    $translateProvider.preferredLanguage('it');
}
})();
