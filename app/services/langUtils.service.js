(function(){
    angular
        .module('app')
        .factory('langUtils',LangUtils);

        LangUtils.$inject = ['$translate','$location','config', 'amMoment', 'DS'];

        function LangUtils($translate,$location,config, amMoment, DS){
            var service = {

                currentLang: 'it',
                langQuery: {
                    where:{
                        lang:{
                            'contains': 'it'
                        }
                    }
                },

                //switch language, update models
                changeLang: function(lang){

                        //RELOAD TAGS
                        // DS.ejectAll('tag');
                        // DS.findAll('tag',
                        //     {where:{
                        //             lang:{
                        //                 'contains': lang
                        //             },
                        //         }
                        //     },{bypassCache:true}
                        // );

                        // SAVE LANGUAGE SETTINGS
                        $translate.use(lang);
                        amMoment.changeLocale(lang);
                        service.currentLang = lang;
                        service.langQuery.where.lang['contains'] = lang;
                        service.watchLang(lang)

                },
                watchLang: function() {
                    // USED FOR SEO (PAGE TITLE, PAGE DESCRIPTION, PAGE META KEYS)
                    return service.currentLang;
                }
            };
            service.changeLang(config.lang.defaultLang);

            return service;

            function isActive(path,fixed) {
              if (fixed==true) return path === $location.path();
              else if ($location.path().substr(0, path.length) == path) return true;
              else return false;
            };
        }

})();
