(function(){
    angular
        .module('app')
        .factory('commonData',commonData);

        commonData.$inject = [];

        function commonData(){
            var service = {
            	homeInterval: undefined
            };

            return service;
        }
})();