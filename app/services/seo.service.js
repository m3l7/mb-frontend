(function(){
    angular
    .module('app')
    .factory('SeoService',SeoService);

    SeoService.$inject = ['$rootScope', 'langUtils', '$routeParams'];

    function SeoService($rootScope, langUtils, $routeParams) {

        var title = {};
        var description = {};
        var metaKeys = {};

        var service = {
            set : function(pageTitle, pageDescription, pageMetaKeys) {
                console.log('seo ', pageTitle)
                title = pageTitle;
                description = pageDescription;
                metaKeys = pageMetaKeys;
            },
            getPageTitle: function() {
                var currentLang = langUtils.currentLang;
                return title;
            },
            getPageDescription: function() {
                var currentLang = langUtils.currentLang;
                return description;
            },
            getPageMetaKeys: function() {
                var currentLang = langUtils.currentLang;
                return metaKeys;
            },
        };

        return service;
    }
})();
