(function(){
	angular
		.module('app')
		.controller('Sidebar',Sidebar);

		Sidebar.$inject = ['$scope','$interval', 'InfoModel', 'TwitModel'];

		function Sidebar($scope,$interval, InfoModel, TwitModel){

			$scope.activeTwitIndex = 0;
			initialize();

			function initialize(){
				InfoModel.findAll();
                InfoModel.bindAll($scope,'info');

				TwitModel.findAll()
                .then(function(twits){
                    $interval(function(){
                        $scope.activeTwitIndex++;
                        if ($scope.activeTwitIndex>=twits.length) $scope.activeTwitIndex = 0;
                    },5000);
                });
                TwitModel.bindAll($scope,'twits');
			}
		}
})();