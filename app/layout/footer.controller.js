(function(){
    angular
        .module('app')
        .controller('Footer',Footer);

        Footer.$inject = ['$scope', '$modal'];

        function Footer($scope, $modal){

                $scope.openInfoModal = function(){

	                var modalInstance = $modal.open({
	                    templateUrl: '/components/infoModal.html',
	                    controller: 'InfoModal'
	                });

            	}
        }
})();