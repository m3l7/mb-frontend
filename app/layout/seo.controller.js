(function(){
    angular
    .module('app')
    .controller('SeoController',SeoController);

    SeoController.$inject = ['$scope', 'langUtils', 'SeoService'];

    function SeoController($scope, langUtils, SeoService){
        $scope.langUtils = langUtils;
        // PAGE TITLE
        $scope.$watch(function () {return SeoService.getPageTitle()} , function() {
            $scope.pageTitle = SeoService.getPageTitle();
            $scope.pageTitle = $scope.pageTitle[langUtils.currentLang];
            $scope.$watch(function () {return langUtils.watchLang()}, function() {
                $scope.pageTitle = SeoService.getPageTitle();
                $scope.pageTitle = $scope.pageTitle[langUtils.currentLang];
            });
        });

        // PAGE DESCRIPTION
        $scope.$watch(function () {return SeoService.getPageDescription()} , function() {
            $scope.pageDescription = SeoService.getPageDescription();
            $scope.pageDescription = $scope.pageDescription[langUtils.currentLang];
            $scope.$watch(function () {return langUtils.watchLang()}, function() {
                $scope.pageDescription = SeoService.getPageDescription();
                $scope.pageDescription = $scope.pageDescription[langUtils.currentLang];
            });
        });

        // PAGE META
        $scope.$watch(function () {return SeoService.getPageMetaKeys()} , function() {
            $scope.pageMeta = SeoService.getPageMetaKeys();
            $scope.pageMeta = $scope.pageMeta[langUtils.currentLang];
            $scope.$watch(function () {return langUtils.watchLang()}, function() {
                $scope.pageMeta = SeoService.getPageMetaKeys();
                $scope.pageMeta = $scope.pageMeta[langUtils.currentLang];
            });
        });
    } 
})();