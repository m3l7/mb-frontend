(function(){
    angular
    .module('app')
    .controller('Header',Header);

    Header.$inject = ['$scope', '$location', '$modal', 'config', 'langUtils', 'CategoryModel','security'];

    function Header($scope, $location, $modal, config, langUtils, CategoryModel, security){
        $scope.langUtils = langUtils;
        $scope.langs = config.lang.langs;
        $scope.logout = logout;
        $scope.security = security;
        $scope.baseUrlAssets = config.API.baseUrlAssets;
        $scope.openPrivacyModal = openPrivacyModal;

        initialize();

        function logout(){
            security.logout();
            $location.path('/');
        }

        function initialize(){
           CategoryModel.findAll();
           CategoryModel.bindAll($scope,'categories');
       }

       function openPrivacyModal(){

        var modalInstance = $modal.open({
            templateUrl: '/components/privacyModal.html',
            controller: 'PrivacyModal'
        });
    }

    $scope.followLink = function(href) {
        host = window.location.host;
        var lowercaseHref = href.toLowerCase();
        if (lowercaseHref.indexOf(host) === -1) {
            window.open(href, "_blank");
        } else {
            window.location.href = href;
        }
    }

}
})();