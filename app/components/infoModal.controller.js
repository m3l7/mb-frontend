(function(){
    angular
        .module('app')
        .controller('InfoModal',InfoModal);

        InfoModal.$inject = ['$scope','$modalInstance'];

        function InfoModal($scope,$modalInstance){

            $scope.ok = function () {

                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };


        }
})();