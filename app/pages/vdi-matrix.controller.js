(function() {
    angular
    .module('app')
    .controller('VdiMatrix',VdiMatrix);

    var mm = "millimiters";
    var columnsSections = "sections and columns";
    var sectionsWheelbase = "sections and wheelbase";

    VdiMatrix.$inject = ['$scope', 'config', 'ProductModel', '$anchorScroll', "langUtils", 'SeoService'];

    function VdiMatrix($scope, config, ProductModel, $anchorScroll, langUtils, SeoService) {


        var pageTitle = {
            en: "VDI - Find your perfect fixing system",
            it: "VDI - Find your perfect fixing system",
            fr: "VDI - Find your perfect fixing system",
            de: "VDI - Find your perfect fixing system",
        };
        var pageDescription = {
            en: "TÜV SÜD verified the conformity to the VDI 6036 of a comprehensive list of MB brackets for all radiator types and applications, for extra safety and confidence for our customers.",
            it: "TÜV SÜD ha testato la conformità alle specifiche della classe 3 della VDI 6036 di una serie di mensole MB per tutte le tipologie di radiatore e di applicazione, per garantire ai nostri clienti la sicurezza del fissaggio in tutte le situazioni.",
            fr: "TÜV SÜD a vérifié la conformité de plusieurs consoles MB à la classe 3 de la VDI 6036, adaptées à chaque modèle de radiateur, afin de donner confiance et sécurité extra à nos clients.",
            de: "Der TÜV SÜD prüft für die verschiedenen MB-Befestigungsmodelle die Konformität nach der Anwendungsklasse 3 der VDI 6036, um für jeden Heizkörper und jede Anwendung unseren Kunden ein Extra an Sicherheit und Vertrauen geben zu können.",
        }
        var pageMeta = {
            en: "Brackets for radiators, Radiator accessories, hooks for radiators, Radiator feet, Radiator attacks, radiators, aluminum radiators, brackets for radiators, radiators d decor, radiator, designer radiators, Radiator accessories, radiators plate, Wall radiators, low-temperature radiators, lamellar radiators, tubular aluminum radiators, radiator accessories, low temperature radiators, house radiators, Wall radiators, radiator wall, Wall radiators, laminated steel radiators, tubular aluminum radiators, high-performance radiators, upper radiator, radiators low temperature steel, replacement radiators, radiator home, Cast iron radiators, aluminum radiators, radiators d decor, aluminum radiators, heater, radiator panels, cast iron radiators, shelves for radiators, Accessories for radiators, tubular heaters, Wall radiators, wall heater, shelves over radiators, shelves above heater, shelf above the radiator, how to disassemble a heater, disassemble heater, aluminum radiators, aluminum radiators, aluminum radiator, aluminum brackets, Mounting aluminum radiators, cast iron radiators, cast iron radiators, cast iron radiators, cast iron radiator, Cast iron radiators, steel or cast iron radiators, Cast iron radiators, cast iron radiators, cast iron radiator, cast iron radiator, Cast iron radiators in columns, Cast iron radiators, Cast iron radiators 3 columns, radiators brackets, brackets, Brackets for air conditioners, online brackets, conditioners brackets, brackets for hydraulic hoses, pipe clamps, brackets for shelves, shelf for washbasin, for shelves media, supports for radiators, shelves for radiators, brackets for heavy shelves, supports for heavy shelves, Drywall shelves, for shelves dowels, shelf for radiator, shelves to walls, for vessels shelves, white shelves, shelves radiators, shelf brackets, long shelves, shelves over radiators, shelf above the radiator, shelves on radiators, shelf radiator, shelves supports, how to disassemble a radiator, how to remove a radiator, disassemble radiator, how to remove a radiator from the wall, remove a radiator, remove radiators, remove the radiator elements, remove aluminum radiator, how to remove the radiators, how to remove the radiator, disassemble radiator valve, replacement radiators, replacing heater, replacing heater, how to replace a radiator, replace a radiator, replace radiators, replace the radiator valve, heated towel rails flag, radiator side ports, replace the radiator with heated towel rails, radiator mounting, Low temperature radiator, how to mount a radiator, mount a radiator, how to mount a radiator, mounted radiator, how to mount the radiator, how to install a heater, install a heater, how to empty a radiator, how to remove a radiator, remove a radiator",
            it: "staffe per termosifoni, accessori per termosifoni, ganci per termosifoni, piedini per termosifoni, attacchi per termosifoni, radiatori, radiatori in alluminio, staffe per radiatori, radiatori d arredo, radiatore, radiatori ,di design, accessori per radiatori, radiatori a piastra, radiatori a parete, radiatori a bassa temperatura, radiatori lamellari, radiatori in alluminio tubolari, accessori radiatori, radiatori bassa temperatura, radiatori casa, radiatori da parete, radiatore a parete, radiatori a muro, radiatori lamellari acciaio, radiatori tubolari alluminio, radiatori ad alto rendimento, radiatore alto, radiatori acciaio bassa temperatura, sostituzione radiatori, radiatore casa, caloriferi in ghisa, caloriferi in alluminio, caloriferi d arredo, caloriferi alluminio, calorifero, pannelli caloriferi, caloriferi ghisa, mensole per caloriferi, accessori per caloriferi, caloriferi tubolari, caloriferi a parete, calorifero a parete, mensole sopra caloriferi, mensole sopra calorifero, mensola sopra calorifero, come smontare un calorifero, smontare calorifero, termosifoni, alluminio termosifoni in alluminio, termosifone alluminio, staffe alluminio, montaggio termosifoni in alluminio, termosifoni in ghisa, termosifoni ghisa, termosifone ghisa, termosifone in ghisa, termosifoni di ghisa, termosifoni acciaio o ghisa, radiatori in ghisa, radiatori ghisa, radiatore ghisa, radiatore in ghisa, radiatori in ghisa a colonne, radiatori di ghisa, radiatori ghisa 3 colonne, staffe termosifoni, staffe, staffe per condizionatori, staffe on line, staffe condizionatori, staffe per tubi idraulici, staffe fissaggio tubi, staffe per mensole, mensola per lavabo, supporti per mensole,mensole per radiatori, mensole per termosifoni, staffe per mensole pesanti, supporti per mensole pesanti, mensole per cartongesso, tasselli per mensole, mensola per termosifone, mensole per pareti, mensole per vasi, mensole bianche, mensole termosifoni, staffe mensole, mensole lunghe, mensole sopra termosifoni, mensola sopra termosifone, mensole sui termosifoni, mensola termosifone, sostegni mensole, come si smonta un termosifone, come smontare un termosifone, smontare termosifone, come smontare un termosifone dal muro, smontare un termosifone, smontare termosifoni, smontare elementi termosifone, smontare termosifone alluminio, come smontare i termosifoni, come smontare il termosifone, smontare valvola termosifone, sostituzione termosifoni, sostituire termosifone, sostituzione termosifone, come sostituire un termosifone, sostituire un termosifone, sostituire termosifoni, sostituire valvola termosifone, termoarredo a bandiera, termoarredo attacchi laterali, sostituire termosifone con termoarredo, montaggio termoarredo, termoarredo bassa temperatura, come montare un termosifone, montare un termosifone, come si monta un termosifone, montare termosifone, come montare termosifone, come installare un termosifone, installare un termosifone, come svuotare un termosifone, come togliere un termosifone, togliere un termosifone",
            fr: "Supports pour radiateurs, Accessoires de radiateur, crochets pour radiateurs, Radiator pieds, attaques de radiateur, radiateurs, radiateurs en aluminium, supports pour radiateurs, radiateurs décor d, radiateur, radiateurs design, Accessoires de radiateur, radiateurs plaque, radiateurs muraux, radiateurs basse température, radiateurs lamellaires, radiateurs en aluminium tubulaires, accessoires de radiateur, radiateurs basse température, radiateurs maison, radiateurs muraux, mur de radiateur, radiateurs muraux, radiateurs en acier laminées, radiateurs en aluminium tubulaires, radiateurs haute performance, radiateur supérieure, radiateurs en acier à basse température, radiateurs de remplacement, maison de radiateur, radiateurs en fonte, radiateurs en aluminium, radiateurs décor d, radiateurs en aluminium, réchauffeur, panneaux radiants, radiateurs en fonte, étagères pour radiateurs, Accessoires pour radiateurs, réchauffeurs tubulaires, radiateurs muraux, chauffage mural, étagères plus de radiateurs, étagères au-dessus de chauffage, étagère au-dessus du radiateur, comment démonter un dispositif de chauffage, chauffe démonter, radiateurs en aluminium, radiateurs en aluminium, un radiateur en aluminium, des supports en aluminium, Montage des radiateurs en aluminium, radiateurs en fonte, radiateurs en fonte, radiateurs en fonte, fonte radiateur fer, radiateurs en fonte, acier ou radiateurs en fonte, radiateurs en fonte, radiateurs en fonte, fonte radiateur fer, fonte radiateur fer, radiateurs en fonte dans les colonnes, radiateurs en fonte, radiateurs en fonte 3 colonnes, radiateurs supports, supports, Supports pour climatiseurs, supports en ligne, Climatiseurs consoles, supports pour tuyaux hydrauliques, des colliers de serrage, supports pour tablettes, étagère pour lavabo, pour étagères médias, supports pour radiateurs, étagères pour radiateurs, supports pour étagères lourdes, supports pour étagères lourds, étagères de cloison sèche, pour étagères Chevilles, étagère pour radiateur, étagères aux murs, pour les navires étagères, étagères blanches, étagères radiateurs, supports d'étagère, longues étagères, étagères plus de radiateurs, étagère au-dessus du radiateur, étagères sur les radiateurs, étagère radiateur, étagères supports, comment démonter un radiateur, comment supprimer un radiateur, radiateur démonter, comment supprimer un radiateur du mur, retirer un radiateur, supprimer les radiateurs, supprimer les éléments de radiateur, retirer le radiateur en aluminium, comment faire pour supprimer les radiateurs, comment faire pour supprimer le radiateur, robinet de radiateur démonter, radiateurs de remplacement, remplacement chauffe, remplacement chauffe, comment remplacer un radiateur, remplacer un radiateur, remplacer les radiateurs, remplacer le robinet de radiateur, serviettes rails drapeau, des orifices latéraux du radiateur, remplacer le radiateur avec porte-serviettes chauffant, le montage du radiateur, Radiateur à basse température, comment monter un radiateur, monter un radiateur, comment monter un radiateur, radiateur monté, comment monter le radiateur, comment installer un appareil de chauffage, installer un chauffe, comment vider un radiateur, comment supprimer un radiateur, retirer un radiateur",
            de: "Halterungen für Heizkörper, Heizkörperzubehör, Haken für Heizkörper, Kühler Füße, Kühler Angriffe, Heizkörper, Aluminiumheizkörper, Halterungen für Heizkörper, Heizkörper d Dekor, Kühler, Design-Heizkörper, Heizkörperzubehör, Heizkörper Platte, Wandstrahler, Niedertemperatur-Heizkörper, Lamellenheizkörper, Aluminium-Rohrheizkörper, Heizkörper Zubehör, Niedertemperaturheizkörper, Haus Heizkörper, Wandstrahler, Heizkörper Wand, Wandstrahler, laminierte Stahlheizkörper, Aluminium-Rohrheizkörper, Hochleistungsstrahler, obere Kühler, Heizkörper Tieftemperaturstahl, Ersatz Heizkörper, Heizkörper nach Hause, Gussradiatoren, Aluminiumheizkörper, Heizkörper d Dekor, Aluminiumheizkörper, Heizung, Abstrahlplatten, Gussradiatoren, Regale für Heizkörper, Zubehör für Heizkörper, Rohrheizkörper, Wandstrahler, Wandheizung, Regale über Heizkörper, Regale über Heizung, Regal über dem Heizkörper, wie eine Heizung zu zerlegen, disassemble Heizung, Aluminiumheizkörper, Aluminiumheizkörper, Aluminiumheizkörper, Aluminium-Halterungen, Montage Aluminium-Heizkörper, Gussradiatoren, Gussradiatoren, Gussradiatoren, Gussheizkörper, Gussradiatoren, Stahl oder Gusseisen-Heizkörper, Gussradiatoren, Gussradiatoren, Gussheizkörper, Gussheizkörper, Gussradiatoren in Spalten, Gussradiatoren, Gussradiatoren 3 Spalten, Heizkörper Klammern, Klammern, Halterungen für Klimaanlagen, Online-Klammern, Aufbereiter Klammern, Halterungen für Hydraulikschläuche, Rohrschellen, Halterungen für Regale, Regal für Waschbecken, für Regale Medien, Stützen für Heizkörper, Regale für Heizkörper, Klammern für schwere Regale, unterstützt für schwere Regale, Trockenbau Regale, für Regale Dübel, Regal für Heizkörper, Regale an der Wand, für Schiffe, Regale, weiße Regale, Regale Heizkörper, Regalstützen, lange Regale, Regale über Heizkörper, Regal über dem Heizkörper, Regale an den Heizkörpern, Regal Heizkörper, Regale Stützen, wie ein Heizkörper zu zerlegen, wie ein Heizkörper zu entfernen, disassemble Heizkörper, wie ein Heizkörper von der Wand zu entfernen, Nehmen Sie den Heizkörper, entfernen Heizkörper, Die Kühlerelemente, entfernen Aluminiumheizkörper, wie die Heizkörper zu entfernen, wie den Kühler zu entfernen, disassemble Heizkörperventil, Ersatz Heizkörper, ersetzen Heizung, ersetzen Heizung, wie ein Heizkörper zu ersetzen, ersetzen einen Kühler, ersetzen Heizkörper, ersetzen Sie das Heizkörperventil, beheizte Handtuchhalter Flagge, Kühler-Seitenanschlüsse, die Heizkörper mit Handtuchwärmer ersetzen, Kühlerbefestigung, Niedrigtemperatur-Heizkörper, wie ein Heizkörper zu montieren, Montage eines Heizkörpers, wie ein Heizkörper zu montieren, montierte Heizkörper, wie die Heizkörper zu montieren, wie eine Heizung zu installieren, Installieren Sie eine Heizung, wie ein Heizkörper zu leeren, wie ein Heizkörper zu entfernen, Nehmen Sie den Heizkörper",
        }

        SeoService.set(pageTitle, pageDescription, pageMeta);


        $scope.baseUrlAssets = config.API.baseUrlAssets;
        $scope.langUtils = langUtils;
        $scope.units = [{id : 0, value : mm}, {id : 1, value : columnsSections}, {id : 2, value : sectionsWheelbase}];

        $scope.vdiMatrix = getVdiMatrix();
        $scope.filteredProducts = [];

        $scope.heatingSystems = getHeatingSystems();
        $scope.heatingSystemSelected = $scope.heatingSystems[0];

        $scope.showMountingSystemSelector = false;
        $scope.mountingSystems = getMountingSystems();
        $scope.mountingSystemSelected = $scope.mountingSystems[0];

        $scope.showMeasurementSystemFields = false;
        // $scope.showMeasurementSystemSelector = false;
        $scope.measurementSystems = getMeasurementSystems();
        $scope.measurementSystemSelected = false;

        $scope.optionsRange = [];

        $scope.vdiMatrixDescription = getVdiMatrixDescription();

        $scope.searchFields = [
        {'h' : 0},
        {'l' : 0},
        {'c' : 0},
        {'s' : 0},
        {'csh' : 0},
        {'n' : 0},
        {'w' : 0},
        ];

        $scope.showForm = false;
        $scope.products = [];
        $scope.baseUrlAssets = config.API.baseUrlAssets;
        $scope.nothingFound = false;
        $scope.allFieldsFilled = false;
        $scope.nothingFoundMessage = {
            it: "Nessun risultato per questa ricerca",
            en: "No results for this search",
            fr: "Pas de résultats pour cette recherche",
            de: "Keine Ergebnisse für diese Suche"
        }

        $scope.searchResults = [];

        $anchorScroll();

        initializeVdiProducts();

        $scope.filterHeatingSystems = function () {
            $scope.filteredProducts = [];
            $scope.searchFields = [];
            if($scope.heatingSystemSelected.id !== 0) {
                $scope.vdiMatrix.forEach(function(vdi) {
                    if (vdi.heatingSystem.id === $scope.heatingSystemSelected.id) {
                        $scope.filteredProducts.push(vdi);
                    }
                });
                $scope.showMountingSystemSelector = true;
                $scope.showMeasurementSystemFields = false;
                if ($scope.heatingSystemSelected.id === 1) {
                    // IF IT'S A CONVECTOR, WE NEED JUST FLOOR MOUNTING SYSTEM, NOT WALL 
                    $scope.mountingSystems.splice(1, 1);
                } else {
                    // ELSE LET CHOOSE FROM ALL MOUNTING SYSTEMS
                    $scope.mountingSystems = getMountingSystems();
                }
                if ($scope.heatingSystemSelected.id === 1 || $scope.heatingSystemSelected.id === 2) {
                    if ($scope.heatingSystemSelected.id === 1) {
                        // IF IT'S A CONVECTOR, SET THE FOLLOWING OPTIONS RANGE
                        $scope.optionsRange = [70, 140, 210, 280];
                    } else {
                        // ELSE IT'S A COLUMN RADIATOR WITH THE FOLLOWING OPTIONS RANGE
                        $scope.optionsRange = [2, 3, 4, 5, 6];
                    }
                }
                $scope.mountingSystemSelected = $scope.mountingSystems[0];
                console.log("mounting systems ", $scope.mountingSystems);
            } else {
                $scope.showMountingSystemSelector = false;
                $scope.showMeasurementSystemFields = false;
            }
        }

        $scope.filterMountingSystems = function () {
            if($scope.mountingSystemSelected.id !== 0) {
                var tempFiltered = [];
                $scope.vdiMatrix.forEach(function(vdi) {
                    if (vdi.mount.id === $scope.mountingSystemSelected.id && vdi.heatingSystem.id === $scope.heatingSystemSelected.id) {
                        tempFiltered.push(vdi);
                    }
                });
                $scope.filteredProducts = tempFiltered;
                $scope.showMeasurementSystemFields = true;
                $scope.filterMeasurementSystems();
            } else {
                $scope.filteredProducts = [];
                $scope.vdiMatrix.forEach(function(vdi) {
                    if (vdi.heatingSystem.id === $scope.heatingSystemSelected.id) {
                        $scope.filteredProducts.push(vdi);
                    }
                });
                $scope.showMeasurementSystemFields = false;
            };
        }

        $scope.filterMeasurementSystems = function () {
            if($scope.filteredProducts.length > 0) {
                $scope.filteredProducts.forEach(function(vdi) {
                    $scope.measurementSystemSelected = vdi.measurementSystem;
                });
            }
        }

        $scope.searchVDI = function() {
            $scope.searchResults = [];

            if ($scope.measurementSystemSelected.id == 1 || $scope.measurementSystemSelected.id == 2) {
                $scope.filteredProducts.forEach(function (vdi){
                    vdi.vdiData.forEach(function (data) {
                        if ($scope.searchFields['h'] >= 1 && $scope.searchFields['l'] >= 1) {
                            $scope.allFieldsFilled = true;
                        } else {
                            $scope.allFieldsFilled = false;
                        }
                        if((data.h[0] <= $scope.searchFields['h'] && data.h[1] >= $scope.searchFields['h']) && (data.l[0] <= $scope.searchFields['l'] && data.l[1] >= $scope.searchFields['l'])) {
                            $scope.allFieldsFilled = true;
                            vdi.name.forEach(function (singleProductName) {
                                $scope.searchResults.push([singleProductName, vdi.notes, data.brackets, $scope.bindVdiProduct(singleProductName), data.related]);
                            });
                        }
                    });
                });
            } else if ($scope.measurementSystemSelected.id == 3) {
                $scope.filteredProducts.forEach(function (vdi){
                    vdi.vdiData.forEach(function (data) {
                        if ($scope.searchFields['c'] >= 1 && $scope.searchFields['s'] >= 1 && $scope.searchFields['csh'] >= 1) {
                            $scope.allFieldsFilled = true;
                        } else {
                            $scope.allFieldsFilled = false;
                        }
                        if((data.c[0] <= $scope.searchFields['c'] && data.c[1] >= $scope.searchFields['c']) && (data.s[0] <= $scope.searchFields['s'] && data.s[1] >= $scope.searchFields['s']) && (data.h[0] <= $scope.searchFields['csh'] && data.h[1] >= $scope.searchFields['csh'])) {
                            $scope.allFieldsFilled = true;
                            vdi.name.forEach(function (singleProductName) {
                                $scope.searchResults.push([singleProductName, vdi.notes, data.brackets, $scope.bindVdiProduct(singleProductName), data.related]);
                            });
                        }
                    });
                });
            } else if ($scope.measurementSystemSelected.id == 4) {
                $scope.filteredProducts.forEach(function (vdi){
                    vdi.vdiData.forEach(function (data) {
                        if ($scope.searchFields['n'] >= 1 && $scope.searchFields['w'] >= 1) {
                            $scope.allFieldsFilled = true;
                        } else {
                            $scope.allFieldsFilled = false;
                        }
                        if((data.n[0] <= $scope.searchFields['n'] && data.n[1] >= $scope.searchFields['n']) && (data.w[0] <= $scope.searchFields['w'] && data.w[1] >= $scope.searchFields['w'])) {
                            $scope.allFieldsFilled = true;
                            vdi.name.forEach(function (singleProductName) {
                                $scope.searchResults.push([singleProductName, vdi.notes, data.brackets, $scope.bindVdiProduct(singleProductName), data.related]);
                            });
                        }
                    });
                });
            } else {
                $scope.allFieldsFilled = false;
            }
            if ($scope.searchResults.length == 0 && $scope.allFieldsFilled) {
                $scope.nothingFound = true;
            } else {
                $scope.nothingFound = false;
            }
        }

        function initializeVdiProducts() {
            ProductModel.findAll().then(function() {
                $scope.showForm = true;
            });
            ProductModel.bindAll($scope,'products');
        }

        $scope.bindVdiProduct = function(productName) {
            var returnProduct = null;
            $scope.products.forEach(function(products) {
                if (products.name.search(productName) !== -1) {
                    returnProduct = products;
                }
            });
            return returnProduct;
        }


        function getHeatingSystems() {
            return [
            {
                id : 0,
                value: {
                    it: "Seleziona il sistema di riscaldamento",
                    en: "Select the heating system",
                    fr: "Sélectionner le système de chauffage",
                    de: "Wählen Sie das Heizsystem",
                },
            },
            {
                id : 1,
                value: {
                    it: "Convettore",
                    en: "Convector",
                    fr: "Convecteur",
                    de: "Konvektor",
                }
            },
            {
                id: 2,
                value: {
                    it: "Radiatore a colonna",
                    en: "Column Radiator",
                    fr: "Radiateur Colonnes",
                    de: "Rohrheizkörper",
                }
            },
            {
                id: 3,
                value: {
                    it: "Piastra in acciaio",
                    en: "Steel panel radiator",
                    fr: "Panneaud en acier",
                    de: "Plattenheizkörper",
                }
            },
            {
                id: 4,
                value: {
                    it: "Radiatore in alluminio",
                    en: "Aluminium Radiator",
                    fr: "Radiateur en aluminium",
                    de: "Aluminium",
                }
            }
            ]
        }

        function getMountingSystems() {
            return [
            {
                id: 0,
                value : {
                    it: "Seleziona il tipo di fissaggio",
                    en: "Select the type of placement   ",
                    fr: "Sélectionner le type de positionnement",
                    de: "Wählen Sie die Befestigungsart",
                }
            },
            {
                id: 1,
                value : {
                    it: "Muro",
                    en: "Wall",
                    fr: "Mur",
                    de: "Wand",
                }
            },
            {
                id: 2,
                value: {
                    it: "Pavimento",
                    en: "Floor",
                    fr: "Sol",
                    de: "Fußboden",
                }
            },
            ]
        }

        function getMeasurementSystems() {
            return [
            {
                id: 0,
                value: {
                    it: "Seleziona l'iunità di misura",
                    en: "Select the unit of measurement",
                    fr: "Sélectionnez l'unité de mesure",
                    de: "Wählen Sie die Maßeinheit",
                },
                fields: []
            },
            {
                id: 1,
                value: {
                    it: "Millimetri (h x w)",
                    en: "mm (h x w)",
                    fr: "mm (h x w)",
                    de: "Millimeter (h x w)",
                },
                fields: [
                {
                    scope: "h",
                    inputType: "dropdown",
                    it: "altezza <span class='lowercase'>(mm)</span>",
                    en: "height <span class='lowercase'>(mm)</span>",
                    fr: "Hauteur <span class='lowercase'>(mm)</span>",
                    de: "Höhe <span class='lowercase'>(mm)</span>",
                },
                {
                    scope: "l",
                    inputType: "number",
                    it: "lunghezza <span class='lowercase'>(mm)</span>",
                    en: "length <span class='lowercase'>(mm)</span>",
                    fr: "longeur <span class='lowercase'>(mm)</span>",
                    de: "LÄNGE <span class='lowercase'>(mm)</span>",
                }
                ]
            },
            {
                id: 2,
                value: {
                    it: "Millimetri (h x w)",
                    en: "mm (h x w)",
                    fr: "mm (h x w)",
                    de: "Millimeter (h x w)",
                },
                fields: [
                {
                    scope: "h",
                    inputType: "number",
                    it: "altezza <span class='lowercase'>(mm)</span>",
                    en: "height <span class='lowercase'>(mm)</span>",
                    fr: "Hauteur <span class='lowercase'>(mm)</span>",
                    de: "Höhe <span class='lowercase'>(mm)</span>",
                },
                {
                    scope: "l",
                    inputType: "number",
                    it: "lunghezza <span class='lowercase'>(mm)</span>",
                    en: "length <span class='lowercase'>(mm)</span>",
                    fr: "longeur <span class='lowercase'>(mm)</span>",
                    de: "LÄNGE <span class='lowercase'>(mm)</span>",
                }
                ]
            },
            {
                id: 3,
                value: {
                    it: "Colonne e Sezioni",
                    en: "Columns & Sections",
                    fr: "des colonnes et des sections",
                    de: "Spalten und Sektionen",
                },
                fields: [
                {
                    scope: "c",
                    inputType: "dropdown",
                    it: "numero di colonne",
                    en: "number of columns",
                    fr: "nombre de colonnes",
                    de: "ANZHAL SÄULEN",
                },
                {
                    scope: "s",
                    inputType: "number",
                    it: "numero di sezioni",
                    en: "number of sections",
                    fr: "nombre de sections",
                    de: "ANZHAL GLIEDER",
                },
                {
                    scope: "csh",
                    inputType: "number",
                    it: "altezza <span class='lowercase'>(mm)</span>",
                    en: "height <span class='lowercase'>(mm)</span>",
                    fr: "Hauteur <span class='lowercase'>(mm)</span>",
                    de: "Höhe <span class='lowercase'>(mm)</span>",
                }
                ]
            },
            {
                id: 4,
                value: {
                    it: "Interasse",
                    en: "Wheelbase",
                    fr: "Empattement",
                    de: "Radstan",
                },
                fields: [
                {
                    scope: "n",
                    inputType: "number",
                    it: "numero di sezioni",
                    en: "number of sections",
                    fr: "nombre de sections",
                    de: "Anzahl glieder",
                },
                {
                    scope: "w",
                    inputType: "number",
                    it: "interasse tra le sezioni <span class='lowercase'>(mm)</span>",
                    en: "basewheel between sections <span class='lowercase'>(mm)</span>",
                    fr: "entraxe entre les sections <span class='lowercase'>(mm)</span>",
                    de: "NABENABSTAND <span class='lowercase'>(mm)</span>",
                }
                ]
            },
            ]
        }

        function getVdiMatrixDescription () {
            return {
                brackets: {
                    it : "Staffe necessarie",
                    en : "Number of brackets needed",
                    fr : "Supports nécessaires",
                    de : "Ergebnisserforderlichen Halterungen",
                },
                related: {
                    it: "Questa soluzione necessita il sistema di fissaggio ",
                    en: "This solution requires the fixing system",
                    fr: "Cette solution nécessite le système de fixation",
                    de: "Diese Lösung erfordert das Befestigungssystem",
                }
            }
        }

        function getVdiMatrix() {
            var heatingSystems = getHeatingSystems();
            var mounts = getMountingSystems();
            var measurementSystem = getMeasurementSystems();
            var c = 1, r = 2, s = 3, a = 4;
            var w = 1, f = 2;
            var mm1 = 1, mm2 = 2, cs = 3, wb = 4;
            // mm1 is for convector, which needs dropdown selection

            return [
            {
                name: ["PKK"],
                measurementSystem : measurementSystem[mm1],
                heatingSystem: heatingSystems[c],
                mount: mounts[f],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [70, 140],
                    l : [400, 800],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [400, 800],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [900, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [900, 1200],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [1300, 2000],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [1300, 2000],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [2100, 2800],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [2100, 2800],
                    brackets : 5,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [2900, 3600],
                    brackets : 5,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [2900, 3600],
                    brackets : 6,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [3700, 4400],
                    brackets : 6,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [3700, 4400],
                    brackets : 7,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [4500, 5200],
                    brackets : 7,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [4500, 5200],
                    brackets : 8,
                    related: false,
                },
                {
                    h : [70, 140],
                    l : [5300, 6000],
                    brackets : 8,
                    related: false,
                },
                {
                    h : [210, 280],
                    l : [5300, 6000],
                    brackets : 9,
                    related: false,
                },
                ],
            },
            {
                name: ["GTX"],
                measurementSystem : measurementSystem[cs],
                heatingSystem: heatingSystems[r],
                mount: mounts[w],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",

                },
                vdiData: [
                {
                    c : [2, 6],
                    s : [1, 20],
                    h : [300, 1000],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [300, 1000],
                    brackets : 3,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [300, 1000],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [300, 1000],
                    brackets : 5,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [1, 20],
                    h : [1100, 2200],
                    brackets : 2,
                    related: ["GTXP"],
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [300, 1000],
                    brackets : 4,
                    related: ["GTXP"],
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [300, 1000],
                    brackets : 5,
                    related: ["GTXP"],
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [300, 1000],
                    brackets : 6,
                    related: ["GTXP"],
                },
                ],
            },
            {
                name: ["GBT"],
                measurementSystem : measurementSystem[cs],
                heatingSystem: heatingSystems[r],
                mount: mounts[w],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",

                },
                vdiData: [
                {
                    c : [2, 5],
                    s : [1, 20],
                    h : [300, 1000],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [2, 5],
                    s : [21, 35],
                    h : [300, 1000],
                    brackets : 3,
                    related: false,
                },
                {
                    c : [2, 5],
                    s : [36, 50],
                    h : [300, 1000],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [2, 5],
                    s : [51, 65],
                    h : [300, 1000],
                    brackets : 6,
                    related: false,
                },
                {
                    c : [6],
                    s : [1, 20],
                    h : [300, 1000],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [6],
                    s : [21, 35],
                    h : [300, 1000],
                    brackets : 3,
                    related: false,
                },
                {
                    c : [6],
                    s : [36, 50],
                    h : [300, 1000],
                    brackets : 6,
                    related: false,
                },
                {
                    c : [6],
                    s : [51, 65],
                    h : [300, 1000],
                    brackets : 6,
                    related: false,
                },
                {
                    c : [2, 4],
                    s : [1, 20],
                    h : [1100, 2200],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [2, 4],
                    s : [21, 35],
                    h : [300, 1000],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [2, 4],
                    s : [36, 50],
                    h : [1100, 2200],
                    brackets : 5,
                    related: false,
                },
                {
                    c : [2, 4],
                    s : [51, 65],
                    h : [1100, 2200],
                    brackets : 6,
                    related: false,
                },
                {
                    c : [5, 6],
                    s : [1, 20],
                    h : [1100, 2200],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [5, 6],
                    s : [21, 35],
                    h : [1100, 2200],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [5, 6],
                    s : [36, 50],
                    h : [1100, 2200],
                    brackets : 5,
                    related: false,
                },
                {
                    c : [5, 6],
                    s : [51, 65],
                    h : [1100, 2200],
                    brackets : 8,
                    related: false,
                },
                ],
            },
            {
                name: ["PGT", "PGT60", "PGT1-60"],
                measurementSystem : measurementSystem[cs],
                heatingSystem: heatingSystems[r],
                mount: mounts[f],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    c : [2, 6],
                    s : [3, 20],
                    h : [300, 900],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [300, 900],
                    brackets : 3,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [300, 900],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [300, 900],
                    brackets : 5,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [3, 20],
                    h : [1000, 2200],
                    brackets : 2,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [1000, 2200],
                    brackets : 3,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [1000, 2200],
                    brackets : 4,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [1000, 2200],
                    brackets : 5,
                    related: ["BG3"],
                },
                ],
            },
            {
                name: ["PGE"],
                measurementSystem : measurementSystem[cs],
                heatingSystem: heatingSystems[r],
                mount: mounts[f],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    c : [2, 6],
                    s : [2, 20],
                    h : [300, 900],
                    brackets : 2,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [300, 900],
                    brackets : 3,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [300, 900],
                    brackets : 4,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [300, 900],
                    brackets : 6,
                    related: false,
                },
                {
                    c : [2, 6],
                    s : [2, 20],
                    h : [1000, 2200],
                    brackets : 2,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [21, 35],
                    h : [1000, 2200],
                    brackets : 4,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [36, 50],
                    h : [1000, 2200],
                    brackets : 5,
                    related: ["BG3"],
                },
                {
                    c : [2, 6],
                    s : [51, 65],
                    h : [1000, 2200],
                    brackets : 6,
                    related: ["BG3"],
                },
                ],
            },
            {
                name: ["PAL"],
                measurementSystem : measurementSystem[wb],
                heatingSystem: heatingSystems[a],
                mount: mounts[f],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    n : [3, 15],
                    w : [350, 500, 600, 700, 800],
                    brackets : 2,
                    related: false,
                },
                {
                    n : [16, 24],
                    w : [350, 500, 600, 700, 800],
                    brackets : 3,
                    related: false,
                },
                ],
            },
            {
                name: ["F7V", "F7VBBT", "F7RB"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[w],
                modelsLimit: ["20", "21", "22", "33"],
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 600],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1400, 1600],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2000, 2300],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1400, 1600],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2000, 2300 ],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                ],
            },
            {
                name: ["F8"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[w],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 900],
                    l : [400, 1400],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 900],
                    l : [1600, 3000],
                    brackets : 3,
                    related: false,
                },
                ],
            },
            {
                name: ["CE-CV"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[w],
                modelsLimit: ["12", "20", "21", "22", "33"],
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 600],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1400, 1600],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1800, 2300],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1400, 1600],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1800, 2300],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                ],
            },
            {
                name: ["F6N"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[w],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 900],
                    l : [400, 1400],
                    brackets : 2,
                    related: ["SC5"],
                },
                {
                    h : [300, 900],
                    l : [1600, 2300],
                    brackets : 3,
                    related: ["SC5"],
                },
                {
                    h : [300, 900],
                    l : [2600, 3000],
                    brackets : 4,
                    related: ["SC5"],
                },
                ],
            },
            {
                name: ["GB1"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[w],
                modelsLimit: false,
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 900],
                    l : [400, 1400],
                    brackets : 2,
                    related: ["SC5"],
                },
                {
                    h : [300, 900],
                    l : [1600, 2300],
                    brackets : 3,
                    related: ["SC5"],
                },
                {
                    h : [300, 900],
                    l : [2600, 3000],
                    brackets : 4,
                    related: ["SC5"],
                },
                ],
            },
            {
                name: ["PK3", "PK1"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[f],
                modelsLimit: ["11", "12", "21", "22", "33"],
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 600],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1400, 1600],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2000, 2300],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1400, 1600],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2000, 2300],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                ],
            },
            {
                name: ["PK23"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[f],
                modelsLimit: ["11", "12", "21", "22", "33"],
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [200, 300],
                    l : [400, 1600],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [200, 300],
                    l : [1800, 2300],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [200, 300],
                    l : [2600, 3000],
                    brackets : 4,
                    related: false,
                },
                ],
            },
            {
                name: ["PX3"],
                measurementSystem : measurementSystem[mm2],
                heatingSystem: heatingSystems[s],
                mount: mounts[f],
                modelsLimit: ["22", "33"],
                notes: {
                    it: "",
                    en: "",
                    fr: "",
                    de: "",
                },
                vdiData: [
                {
                    h : [300, 600],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1400, 1600],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2000, 2300],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [300, 600],
                    l : [2400, 3000],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [400, 1200],
                    brackets : 2,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1400, 1600],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [1800],
                    brackets : 3,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2000, 2300],
                    brackets : 4,
                    related: false,
                },
                {
                    h : [700, 900],
                    l : [2400, 3000],
                    brackets : 4,
                    related: false,
                },
                ],
            },
            ]
        }

    }
})();