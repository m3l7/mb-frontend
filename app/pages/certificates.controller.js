(function(){
    angular
        .module('app')
        .controller('Certificates',Certificates);

        Certificates.$inject = ['$scope', 'config', '$anchorScroll','langUtils'];

        function Certificates($scope, config, $anchorScroll,langUtils){
        	$scope.baseUrlAssets = config.API.baseUrlAssets;
        	$scope.langUtils = langUtils;

        	$anchorScroll();
        }
})();