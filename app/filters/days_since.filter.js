(function(){
    angular
        .module('app')
        .filter('daysSince', ['$window', function ($window) {
            'use strict';

            return function (value, format) {
                if (!!value) value = new Date() - new Date(value);  
                return Math.floor($window.moment.duration(value, 'milliseconds').asDays());
            };
        }]);
})();


    