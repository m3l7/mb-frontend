(function(){
	angular
		.module('app')
		.filter('truncate', truncate);

		function truncate() {
		  return function (item) {
		  	if ((!item) || (!item.length)) return item;
		    else return item.substring(0,20)+'...';
		  };
		};

})();