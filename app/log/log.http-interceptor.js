// Log HTTP requests to browser's console

(function(){

    angular
        .module('app')
        .factory('logHttpInterceptor',logHttpInterceptor);
    angular
        .module('app')
        .config(configInterceptor);

    logHttpInterceptor.$inject = ['$log','$q', 'config']

    function logHttpInterceptor($log,$q,config){
        return {
            request: function(req){
                if (config.logging.http){
                    var params = '';

                    if (!!req.params) {
                        var params = '?';
                        Object.keys(req.params).forEach(function(k){
                            params += k+"="+JSON.stringify(req.params[k])+"&";
                        })
                        params = params.substring(0, params.length - 1);
                    }
                    $log.info(req.method+": "+req.url+params); 
                    if (!!req.data) $log.log(JSON.stringify(req.data));
                } 
                return req;
            },
            response: function(response){
                // console.log(response.status+' lol')
                if (response.status=='400') $log.error(response.data)
                else if (response.status=='401') $log.error('401: Unauthorized')
                return response;
            },
            responseError: function(res){
                // debugger
            }
        };
    }

    function configInterceptor($httpProvider){
        $httpProvider.interceptors.push('logHttpInterceptor');
    }
})();